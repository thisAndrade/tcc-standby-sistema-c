﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PFC___StandBy_CSharp.Forms;

namespace PFC___StandBy_CSharp.MsgBox
{
    class MensagensSucesso
    {
        //ORDENS DE SERVICO
        public void InserirServicoSucesso()
        {
            //MessageBox.Show("Serviço inserido com sucesso!", "SUCESSO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            form_ALERT message = new form_ALERT("Serviço inserido com sucesso!", form_ALERT.AlertType.Sucesso);
            message.Show();
        }

        public void AlterarServicoSucesso()
        {
            form_ALERT message = new form_ALERT("Serviço alterado com sucesso!", form_ALERT.AlertType.Sucesso);
            message.Show();
        }

        public void DeletarServicoSucesso()
        {
            form_ALERT message = new form_ALERT("Serviço deletado com sucesso!", form_ALERT.AlertType.Sucesso);
            message.Show();
        }




        //CADASTRO DE CLIENTE
        public void InserirClienteSucesso()
        {
            form_ALERT message = new form_ALERT("Cliente inserido com sucesso!", form_ALERT.AlertType.Sucesso);
            message.Show();
        }
    }
}

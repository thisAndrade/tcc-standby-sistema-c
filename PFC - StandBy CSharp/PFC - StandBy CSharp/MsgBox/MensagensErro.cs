﻿using PFC___StandBy_CSharp.SqlDbConnect;
using PFC___StandBy_CSharp.Forms;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PFC___StandBy_CSharp.MsgBox
{
    class MensagensErro : conexao
    {
        // EEROS DA TELA DE ORDENS DE SERVICO
        public void ComboBoxClienteErroEmPreencher(Exception e)
        {
            //MessageBox.Show("Erro ao tentar preencher os clientes na combobox!\n\n\nERRO: "+e+"","ERROR",MessageBoxButtons.OK,MessageBoxIcon.Information);
            form_ALERT message = new form_ALERT("(OR-SV01)Erro ao tentar preencher os clientes na combobox!", form_ALERT.AlertType.Erro);
            message.Show();
        }

        public void ErroBuscarIdCliente(Exception e)
        {
            //MessageBox.Show("Erro ao tentar buscar a ID do cliente\n\n\nERRO: " + e + "", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
            form_ALERT message = new form_ALERT("(OR-SV02)Erro ao tentar buscar a ID do cliente!", form_ALERT.AlertType.Erro);
            message.Show();
        }

        public void ErroInserirServico(Exception e)
        {
            //MessageBox.Show("Erro ao tentar inserir servico.\n\n\nERRO: " + e + "", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
            form_ALERT message = new form_ALERT("(OR-SV03)Erro ao tentar inserir serviço!", form_ALERT.AlertType.Erro);
            message.Show();
        }

        public void ErroPreencherTabelaServicos(Exception e)
        {
            form_ALERT message = new form_ALERT("(OR-SV04)Erro ao tentar preencher a tabela de serviços!", form_ALERT.AlertType.Erro);
            message.Show();
        }

        public void ErroPreencherServicosCliente(Exception e)
        {
            form_ALERT message = new form_ALERT("(OR-SV05)Erro ao preencher os serviços do cliente solicitado!", form_ALERT.AlertType.Erro);
            message.Show();
        }

        public void ErroAlterarServico(Exception e)
        {
            form_ALERT message = new form_ALERT("(OR-SV06)Erro ao alterar o serviço do cliente!", form_ALERT.AlertType.Erro);
            message.Show();
        }

        public void ErroDeletarServico(Exception e)
        {
            form_ALERT message = new form_ALERT("(OR-SV07)Erro ao deletar o serviço do cliente!", form_ALERT.AlertType.Erro);
            message.Show();
        }




        //ERROS DA TELA DE CADASTRO DE CLIENTE
        public void ErroInserirCliente(Exception e)
        {
            form_ALERT message = new form_ALERT("Erro ao tentar inserir o cliente!", form_ALERT.AlertType.Erro);
            message.Show();
        }
    }
}

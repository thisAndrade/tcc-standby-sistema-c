﻿using PFC___StandBy_CSharp.Graficos;
using System;
using System.Windows.Forms;

namespace PFC___StandBy_CSharp.Forms
{
    public partial class form_ServicosPorMes : Form
    {
        private int anoAtual = DateTime.Now.Year;
        private SemanaDoAno semanaAno = new SemanaDoAno();
        public form_ServicosPorMes()
        {
            InitializeComponent();
            semanaAno.PreencherGraficoServicosPorMes(chart1, anoAtual, lblAno);
        }

        private void iconClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

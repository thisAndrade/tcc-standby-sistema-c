﻿using PFC___StandBy_CSharp.PreencherComponentes;
using PFC___StandBy_CSharp.Graficos;
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;

namespace PFC___StandBy_CSharp.Forms
{
    public partial class form_StandBy : Form
    {
        SemanaDoAno semanaAno = new SemanaDoAno();
        private Form currentChildForm;
        int anoAtual = DateTime.Now.Year;
        int mesAtual = DateTime.Now.Month;
        int[] corGeral = new int[3] { 0, 0, 0 };
        public form_StandBy()
        {
            InitializeComponent();
            //chamarRED.Start();
            
            IniciarPainelCor();
            iniciarFormTransicao.ShowAsyc(this);
            semanaAno.VerificarServicosSemanais(bunifuDataViz1, anoAtual, lblQntServicosSemanais);
            semanaAno.VerificarServicosMensais(bunifuDataViz2, mesAtual, lblQntServicosMensais);
        }

        private void IniciarPainelCor()
        {
            corGeral[0] = track_RED.Value;
            lblRED.Text = track_RED.Value.ToString();
            //lblRED.ForeColor = Color.FromArgb(track_RED.Value, 0, 0);

            corGeral[1] = track_GREEN.Value;
            lblGREEN.Text = track_GREEN.Value.ToString();
            //lblGREEN.ForeColor = Color.FromArgb(0, track_GREEN.Value, 0);

            corGeral[2] = track_BLUE.Value;
            lblBLUE.Text = track_BLUE.Value.ToString();
            //lblBLUE.ForeColor = Color.FromArgb(0, 0, track_BLUE.Value);

            panel_CorGeral.BackColor = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
        }

        //public static void CheckSpeed()
        //{
        //    NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
        //    foreach (NetworkInterface adapter in adapters.Where(a => a.OperationalStatus == OperationalStatus.Up))
        //    {
        //        Console.WriteLine("\nDescription: {0} \nId: {1} \nIsReceiveOnly: {2} \nName: {3} \nNetworkInterfaceType: {4} " +
        //            "\nOperationalStatus: {5} " +
        //            "\nSpeed (bits per second): {6} " +
        //            "\nSpeed (kilobits per second): {7} " +
        //            "\nSpeed (megabits per second): {8} " +
        //            "\nSpeed (gigabits per second): {9} " +
        //            "\nSupportsMulticast: {10}",
        //            adapter.Description,
        //            adapter.Id,
        //            adapter.IsReceiveOnly,
        //            adapter.Name,
        //            adapter.NetworkInterfaceType,
        //            adapter.OperationalStatus,
        //            adapter.Speed,
        //            adapter.Speed / 1000,
        //            adapter.Speed / 1000 / 1000,
        //            adapter.Speed / 1000 / 1000 / 1000,
        //            adapter.SupportsMulticast);

        //        var ipv4Info = adapter.GetIPv4Statistics();
        //        Console.WriteLine("OutputQueueLength: {0}", ipv4Info.OutputQueueLength);
        //        Console.WriteLine("BytesReceived: {0}", ipv4Info.BytesReceived);
        //        Console.WriteLine("BytesSent: {0}", ipv4Info.BytesSent);

        //        if (adapter.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 ||
        //            adapter.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
        //        {

        //            MessageBox.Show($"*** Ethernet or WiFi Network - Speed (bits per seconde): {adapter.Speed / 1000 / 1000}");
        //        }
        //    }
        //}

        //public void ConnSpeed()
        //{
        //    Stopwatch stopwatch = new Stopwatch();

        //    stopwatch.Reset();
        //    stopwatch.Start();

        //    WebClient webClient = new WebClient();
        //    byte[] bytes = webClient.DownloadData("https://www.codeproject.com");

        //    stopwatch.Stop();

        //    double seconds = stopwatch.Elapsed.TotalSeconds;

        //    double speed = bytes.Count() / seconds;

        //    //MessageBox.Show(string.Format("Your speed: {0} bytes per second.", speed / 1000));
        //    //lblConexao.Text = (speed/1000).ToString("F2");
        //}

        private void iconClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void iconMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void iconRestaure_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            iconMaximize.Visible = true;
            iconRestaure.Visible = false;
        }

        private void iconMaximize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;
            iconMaximize.Visible = false;
            iconRestaure.Visible = true;
            //ordensserv.lblSituacao.Anchor = AnchorStyles.None;
        }

        private void iconMenu_Click(object sender, EventArgs e)
        {
            if (panelMenu.Width == 187)
            {
                panelMenu.Visible = false;
                panelMenu.Width = 62;
                panel1.Width = 74;
                //panelCentral.Width = 1209;
                lblBemVindo.Visible = false;
                lblSS.Visible = true;
                animationMenuHide1.Show(panelMenu);
            }
            else
            {
                panelMenu.Visible = false;
                panelMenu.Width = 187;
                panel1.Width = 199;
                //panelCentral.Width = 1084;
                lblBemVindo.Visible = true;
                lblSS.Visible = false;
                animationMenuHide1.Show(panelMenu);
            }
        }

        private void OpenChildForm(Form formFilho)
        {
            //open only form
            if (currentChildForm != null)
            {
                currentChildForm.Close();
            }
            currentChildForm = formFilho;
            //End
            formFilho.TopLevel = false;
            formFilho.FormBorderStyle = FormBorderStyle.None;
            formFilho.Dock = DockStyle.Fill;
            panelCentral.Controls.Add(formFilho);
            panelCentral.Tag = formFilho;
            formFilho.BringToFront();
            formFilho.Show();
            //lblAbaAtual.Text = formFilho.Text;
        }

        private void btnServicos_Click(object sender, EventArgs e)
        {
            OpenChildForm(new form_OrdensServ(corGeral));
            imgbuttonTitulo.Image = Image.FromFile(@"..\\..\\Resources\\TITULO ORDENS DE SERVICO.png");
        }

        private void btnClientes_Click(object sender, EventArgs e)
        {
            OpenChildForm(new form_CadastroClientes(corGeral));
            imgbuttonTitulo.Image = Image.FromFile(@"..\\..\\Resources\\TITULO CADASTRO CLIENTES.png");
        }

        //private void chamarRED_Tick(object sender, EventArgs e)
        //{
        //    if (bunifuColorTransition1.ProgessValue < 100)
        //    {
        //        bunifuColorTransition1.ProgessValue += 5;
        //        lblSTANDBY.ForeColor = bunifuColorTransition1.Value;
        //        //bunifuCards3.color = bunifuColorTransition1.Value;
        //        //CheckSpeed()
        //    }
        //    else
        //    {
        //        chamarRED.Stop();
        //        chamarWHITE.Start();
        //    }
        //}

        //private void chamarWHITE_Tick(object sender, EventArgs e)
        //{
        //    if (bunifuColorTransition1.ProgessValue > 0)
        //    {
        //        bunifuColorTransition1.ProgessValue -= 5;
        //        lblSTANDBY.ForeColor = bunifuColorTransition1.Value;
        //        //bunifuCards3.color = bunifuColorTransition1.Value;
        //        //CheckSpeed();
        //        //ConnSpeed();
        //    }
        //    else
        //    {
        //        chamarWHITE.Stop();
        //        chamarRED.Start();
        //        //ConnSpeed();
        //    }
        //}

        private void btnServicosPorMes_Click(object sender, EventArgs e)
        {
            form_ServicosPorMes servMes = new form_ServicosPorMes();
            servMes.Show();
        }

        private void bunifuTileButton3_Click(object sender, EventArgs e)
        {
            DialogResult dialogo = MessageBox.Show("Deseja desligar o computador?", "DESLIGAR", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (dialogo == DialogResult.Yes)
            {
                MessageBox.Show("Funcao desativada no momento para evitar problemas.");
                //Process.Start("shutdown", "/s /t 0");
            }
        }

        private void mudarCorGeral_Tick(object sender, EventArgs e)
        {
            corGeral[0] = track_RED.Value;
            lblRED.Text = track_RED.Value.ToString();
            //lblRED.ForeColor = Color.FromArgb(track_RED.Value, 0, 0);

            corGeral[1] = track_GREEN.Value;
            lblGREEN.Text = track_GREEN.Value.ToString();
            //lblGREEN.ForeColor = Color.FromArgb(0, track_GREEN.Value, 0);

            corGeral[2] = track_BLUE.Value;
            lblBLUE.Text = track_BLUE.Value.ToString();
            //lblBLUE.ForeColor = Color.FromArgb(0, 0, track_BLUE.Value);

            panel_CorGeral.BackColor = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);

        }

        private void btnMudarCor_Click(object sender, EventArgs e)
        {
            btnServicosPorMes.colorActive = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            btnDesligarPc.colorActive = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            //bunifuCards1.color = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            bunifuCards2.color = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            bunifuCards3.color = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            bunifuCards8.color = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            panelMenu.GradientBottomLeft = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            panelMenu.GradientBottomRight = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            panelMenu.GradientTopLeft = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            bunifuTileButton1.colorActive = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            bunifuTileButton2.colorActive = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
        }

        public void ResetarCor()
        {
            btnServicosPorMes.colorActive = Color.FromArgb(255, 0, 103);
            btnDesligarPc.colorActive = Color.FromArgb(255, 0, 103);
            //bunifuCards1.color = Color.FromArgb(255, 0, 103);
            bunifuCards2.color = Color.FromArgb(255, 0, 103);
            bunifuCards3.color = Color.FromArgb(255, 0, 103);
            bunifuCards8.color = Color.FromArgb(255, 0, 103);
            panelMenu.GradientBottomLeft = Color.FromArgb(255, 0, 103);
            panelMenu.GradientBottomRight = Color.FromArgb(255, 0, 103);
            panelMenu.GradientTopLeft = Color.FromArgb(255, 0, 103);
            bunifuTileButton1.colorActive = Color.FromArgb(255, 0, 103);
            bunifuTileButton2.colorActive = Color.FromArgb(255, 0, 103);

            track_RED.Value = 255;
            track_GREEN.Value = 0;
            track_BLUE.Value = 103;
            panel_CorGeral.BackColor = Color.FromArgb(255, 0, 103);
        }

        private void track_RED_ValueChanged(object sender, EventArgs e)
        {
            mudarCorGeral.Start();
        }

        private void track_RED_ValueChangeComplete(object sender, EventArgs e)
        {
            mudarCorGeral.Stop();
        }

        private void track_GREEN_ValueChanged(object sender, EventArgs e)
        {
            mudarCorGeral.Start();
        }

        private void track_GREEN_ValueChangeComplete(object sender, EventArgs e)
        {
            mudarCorGeral.Stop();
        }

        private void track_BLUE_ValueChanged(object sender, EventArgs e)
        {
            mudarCorGeral.Start();
        }

        private void track_BLUE_ValueChangeComplete(object sender, EventArgs e)
        {
            mudarCorGeral.Stop();
        }

        private void btnResetarCor_Click(object sender, EventArgs e)
        {
            ResetarCor();
        }

        private void imgbuttonTitulo_Click(object sender, EventArgs e)
        {
            if (currentChildForm != null)
            {
                currentChildForm.Close();
            }
            imgbuttonTitulo.Image = Image.FromFile(@"..\\..\\Resources\\TITULO STANDBY SYSTEM.png");
        }
    }
}

﻿using PFC___StandBy_CSharp.Dados;
using PFC___StandBy_CSharp.SqlDbConnect;
using PFC___StandBy_CSharp.PreencherComponentes.Tela_2___Cadastro_Clientes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PFC___StandBy_CSharp.Forms
{
    public partial class form_CadastroClientes : Form
    {
        PreencherTableClientes preencherClientes = new PreencherTableClientes();
        int[] corGeral = new int[3] { 0, 0, 0 };
        public form_CadastroClientes(int[] corRGB)
        {
            InitializeComponent();
            preencherClientes.Preencher(table_Clientes);
            corGeral = corRGB;
            MudarTodasCores();
        }

        public void MudarTodasCores()
        {
            txtNomeCliente.LineIdleColor = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            txtCPFCliente.LineIdleColor = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            txtTelefoneCliente.LineIdleColor = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            txtPesquisarCADCliente.BorderColorIdle = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            btnCadastrarCliente.IconColor = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            table_Clientes.RowsDefaultCellStyle.SelectionBackColor = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
        }

        private void txtNomeCliente_Enter(object sender, EventArgs e)
        {
            if (txtNomeCliente.Text == "Nome do Cliente")
            {
                txtNomeCliente.Text = "";
                txtNomeCliente.Font = new Font(txtNomeCliente.Font, FontStyle.Regular);
                txtNomeCliente.LineIdleColor = Color.White;
                txtNomeCliente.ForeColor = Color.White;
            }
        }

        private void txtNomeCliente_Leave(object sender, EventArgs e)
        {
            if (txtNomeCliente.Text == "")
            {
                txtNomeCliente.Text = "Nome do Cliente";
                txtNomeCliente.Font = new Font(txtNomeCliente.Font, FontStyle.Italic);
                txtNomeCliente.ForeColor = Color.Silver;
                txtNomeCliente.LineIdleColor = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            }
        }

        private void txtCPFCliente_Enter(object sender, EventArgs e)
        {
            if (txtCPFCliente.Text == "CPF do Cliente")
            {
                txtCPFCliente.Text = "";
                txtCPFCliente.Font = new Font(txtCPFCliente.Font, FontStyle.Regular);
                txtCPFCliente.LineIdleColor = Color.White;
                txtCPFCliente.ForeColor = Color.White;
            }
        }

        private void txtCPFCliente_Leave(object sender, EventArgs e)
        {
            if (txtCPFCliente.Text == "")
            {
                txtCPFCliente.Text = "CPF do Cliente";
                txtCPFCliente.Font = new Font(txtCPFCliente.Font, FontStyle.Italic);
                txtCPFCliente.ForeColor = Color.Silver;
                txtCPFCliente.LineIdleColor = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            }
        }

        private void txtTelefoneCliente_Enter(object sender, EventArgs e)
        {
            if (txtTelefoneCliente.Text == "Telefone do Cliente")
            {
                txtTelefoneCliente.Text = "";
                txtTelefoneCliente.Font = new Font(txtTelefoneCliente.Font, FontStyle.Regular);
                txtTelefoneCliente.LineIdleColor = Color.White;
                txtTelefoneCliente.ForeColor = Color.White;
            }
        }

        private void txtTelefoneCliente_Leave(object sender, EventArgs e)
        {
            if (txtTelefoneCliente.Text == "")
            {
                txtTelefoneCliente.Text = "Telefone do Cliente";
                txtTelefoneCliente.Font = new Font(txtTelefoneCliente.Font, FontStyle.Italic);
                txtTelefoneCliente.ForeColor = Color.Silver;
                txtTelefoneCliente.LineIdleColor = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            }
        }

        private void txtPesquisarCADCliente_Enter(object sender, EventArgs e)
        {
            if (txtPesquisarCADCliente.Text == "Digite o nome do cliente que deseja buscar os serviços")
            {
                txtPesquisarCADCliente.Text = "";
                txtPesquisarCADCliente.Font = new Font(txtPesquisarCADCliente.Font, FontStyle.Regular);
                txtPesquisarCADCliente.BorderColorIdle = Color.White;
                txtPesquisarCADCliente.ForeColor = Color.White;
            }
        }

        private void txtPesquisarCADCliente_Leave(object sender, EventArgs e)
        {
            if (txtPesquisarCADCliente.Text == "")
            {
                txtPesquisarCADCliente.Text = "Digite o nome do cliente que deseja buscar os serviços";
                txtPesquisarCADCliente.Font = new Font(txtPesquisarCADCliente.Font, FontStyle.Italic);
                txtPesquisarCADCliente.ForeColor = Color.Silver;
                txtPesquisarCADCliente.BorderColorIdle = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
            }
        }

        private void btnCadastrarCliente_Click(object sender, EventArgs e)
        {
            //Instanciando InserirDados
            InserirDados id = new InserirDados();

            //Pegar os dados dos campos
            String nome = txtNomeCliente.Text.ToString();
            String cpf = txtCPFCliente.Text.ToString();
            String tel = txtTelefoneCliente.Text.ToString();

            id.InserirCliente(nome, cpf, tel);

            //Limpar os campos
            txtPesquisarCADCliente.Text = "Digite o nome do cliente que deseja buscar os serviços";
            txtPesquisarCADCliente.Font = new Font(txtPesquisarCADCliente.Font, FontStyle.Italic);
            txtPesquisarCADCliente.ForeColor = Color.Silver;
            txtPesquisarCADCliente.BorderColorIdle = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);

            txtTelefoneCliente.Text = "Telefone do Cliente";
            txtTelefoneCliente.Font = new Font(txtTelefoneCliente.Font, FontStyle.Italic);
            txtTelefoneCliente.ForeColor = Color.Silver;
            txtTelefoneCliente.LineIdleColor = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);

            txtCPFCliente.Text = "CPF do Cliente";
            txtCPFCliente.Font = new Font(txtCPFCliente.Font, FontStyle.Italic);
            txtCPFCliente.ForeColor = Color.Silver;
            txtCPFCliente.LineIdleColor = Color.FromArgb(corGeral[0], corGeral[1], corGeral[2]);
        }

        private void txtPesquisarCADCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

            }
        }
    }
}

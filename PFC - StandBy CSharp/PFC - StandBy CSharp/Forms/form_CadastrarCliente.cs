﻿using PFC___StandBy_CSharp.Dados;
using PFC___StandBy_CSharp.SqlDbConnect;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PFC___StandBy_CSharp.Forms
{
    public partial class form_CadastrarCliente : Form
    {
        public form_CadastrarCliente()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnCadastrarCliente_Click(object sender, EventArgs e)
        {
            //Instanciando InserirDados
            InserirDados id = new InserirDados();

            //Pegar os dados dos campos
            String nome = txtNomeCliente.Text.ToString();
            String cpf = txtCpfCliente.Text.ToString();
            String tel = txtTelCliente.Text.ToString();

            id.InserirCliente(nome, cpf, tel);

            //Limpar os campos
            txtNomeCliente.Text = "";
            txtCpfCliente.Text = "";
            txtTelCliente.Text = "";
            conexao con = new conexao();
            SqlConnection teste = con.OpenConnection();

            if(teste.State != null)
            {
                lblStatus.Text = teste.State.ToString();
                MessageBox.Show("CONECTADO");
                con.CloseConnection();
                lblStatus.Text = teste.State.ToString();
            }

        }
    }
}

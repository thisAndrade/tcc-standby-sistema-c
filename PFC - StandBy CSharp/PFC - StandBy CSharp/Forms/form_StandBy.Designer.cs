﻿namespace PFC___StandBy_CSharp.Forms
{
    partial class form_StandBy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation2 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form_StandBy));
            this.panelTopStandBy = new System.Windows.Forms.Panel();
            this.imgbuttonTitulo = new Bunifu.Framework.UI.BunifuImageButton();
            this.iconMinimize = new FontAwesome.Sharp.IconPictureBox();
            this.iconMaximize = new FontAwesome.Sharp.IconPictureBox();
            this.iconClose = new FontAwesome.Sharp.IconPictureBox();
            this.iconRestaure = new FontAwesome.Sharp.IconPictureBox();
            this.moverForm = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.iconMenu = new FontAwesome.Sharp.IconPictureBox();
            this.panelMenu = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.btnLucros = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnOrcamentos = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnConcluidos = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnClientes = new Bunifu.Framework.UI.BunifuFlatButton();
            this.lblSS = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btnServicos = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.lblBemVindo = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.ArredondarForm = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.ArredondarMenu = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bunifuCards2 = new Bunifu.Framework.UI.BunifuCards();
            this.bunifuDataViz2 = new Bunifu.DataViz.WinForms.BunifuDataViz();
            this.lblQntServicosMensais = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.lblQntServicosSemanais = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuDataViz1 = new Bunifu.DataViz.WinForms.BunifuDataViz();
            this.bunifuCards3 = new Bunifu.Framework.UI.BunifuCards();
            this.bunifuCards7 = new Bunifu.Framework.UI.BunifuCards();
            this.btnDesligarPc = new Bunifu.Framework.UI.BunifuTileButton();
            this.bunifuCards6 = new Bunifu.Framework.UI.BunifuCards();
            this.bunifuTileButton2 = new Bunifu.Framework.UI.BunifuTileButton();
            this.bunifuCards5 = new Bunifu.Framework.UI.BunifuCards();
            this.bunifuTileButton1 = new Bunifu.Framework.UI.BunifuTileButton();
            this.bunifuCards4 = new Bunifu.Framework.UI.BunifuCards();
            this.btnServicosPorMes = new Bunifu.Framework.UI.BunifuTileButton();
            this.bunifuCards8 = new Bunifu.Framework.UI.BunifuCards();
            this.lblBLUE = new System.Windows.Forms.Label();
            this.lblGREEN = new System.Windows.Forms.Label();
            this.lblRED = new System.Windows.Forms.Label();
            this.btnResetarCor = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnMudarCor = new Bunifu.Framework.UI.BunifuImageButton();
            this.track_BLUE = new Bunifu.Framework.UI.BunifuSlider();
            this.track_GREEN = new Bunifu.Framework.UI.BunifuSlider();
            this.track_RED = new Bunifu.Framework.UI.BunifuSlider();
            this.panel_CorGeral = new System.Windows.Forms.Panel();
            this.panelCentral = new System.Windows.Forms.Panel();
            this.bunifuCards1 = new Bunifu.Framework.UI.BunifuCards();
            this.bunifuColorTransition1 = new Bunifu.Framework.UI.BunifuColorTransition(this.components);
            this.mudarCorGeral = new System.Windows.Forms.Timer(this.components);
            this.iniciarFormTransicao = new Bunifu.Framework.UI.BunifuFormFadeTransition(this.components);
            this.animationMenuShow1 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.animationMenuHide1 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.panelTopStandBy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgbuttonTitulo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconMinimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconMaximize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconRestaure)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconMenu)).BeginInit();
            this.panelMenu.SuspendLayout();
            this.bunifuCards2.SuspendLayout();
            this.bunifuCards3.SuspendLayout();
            this.bunifuCards7.SuspendLayout();
            this.bunifuCards6.SuspendLayout();
            this.bunifuCards5.SuspendLayout();
            this.bunifuCards4.SuspendLayout();
            this.bunifuCards8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnResetarCor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMudarCor)).BeginInit();
            this.panelCentral.SuspendLayout();
            this.bunifuCards1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTopStandBy
            // 
            this.panelTopStandBy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.panelTopStandBy.Controls.Add(this.imgbuttonTitulo);
            this.panelTopStandBy.Controls.Add(this.iconMinimize);
            this.panelTopStandBy.Controls.Add(this.iconMaximize);
            this.panelTopStandBy.Controls.Add(this.iconClose);
            this.panelTopStandBy.Controls.Add(this.iconRestaure);
            this.animationMenuHide1.SetDecoration(this.panelTopStandBy, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.panelTopStandBy, BunifuAnimatorNS.DecorationType.None);
            this.panelTopStandBy.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTopStandBy.Location = new System.Drawing.Point(74, 0);
            this.panelTopStandBy.Name = "panelTopStandBy";
            this.panelTopStandBy.Size = new System.Drawing.Size(1206, 55);
            this.panelTopStandBy.TabIndex = 0;
            // 
            // imgbuttonTitulo
            // 
            this.imgbuttonTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.animationMenuHide1.SetDecoration(this.imgbuttonTitulo, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.imgbuttonTitulo, BunifuAnimatorNS.DecorationType.None);
            this.imgbuttonTitulo.Image = ((System.Drawing.Image)(resources.GetObject("imgbuttonTitulo.Image")));
            this.imgbuttonTitulo.ImageActive = null;
            this.imgbuttonTitulo.Location = new System.Drawing.Point(756, 0);
            this.imgbuttonTitulo.Name = "imgbuttonTitulo";
            this.imgbuttonTitulo.Size = new System.Drawing.Size(316, 52);
            this.imgbuttonTitulo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgbuttonTitulo.TabIndex = 8;
            this.imgbuttonTitulo.TabStop = false;
            this.imgbuttonTitulo.Zoom = 10;
            this.imgbuttonTitulo.Click += new System.EventHandler(this.imgbuttonTitulo_Click);
            // 
            // iconMinimize
            // 
            this.iconMinimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconMinimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.animationMenuHide1.SetDecoration(this.iconMinimize, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.iconMinimize, BunifuAnimatorNS.DecorationType.None);
            this.iconMinimize.IconChar = FontAwesome.Sharp.IconChar.WindowMinimize;
            this.iconMinimize.IconColor = System.Drawing.Color.White;
            this.iconMinimize.IconSize = 29;
            this.iconMinimize.Location = new System.Drawing.Point(1102, 13);
            this.iconMinimize.Name = "iconMinimize";
            this.iconMinimize.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.iconMinimize.Size = new System.Drawing.Size(29, 29);
            this.iconMinimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.iconMinimize.TabIndex = 4;
            this.iconMinimize.TabStop = false;
            this.iconMinimize.Click += new System.EventHandler(this.iconMinimize_Click);
            // 
            // iconMaximize
            // 
            this.iconMaximize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconMaximize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.animationMenuHide1.SetDecoration(this.iconMaximize, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.iconMaximize, BunifuAnimatorNS.DecorationType.None);
            this.iconMaximize.IconChar = FontAwesome.Sharp.IconChar.WindowMaximize;
            this.iconMaximize.IconColor = System.Drawing.Color.White;
            this.iconMaximize.IconSize = 29;
            this.iconMaximize.Location = new System.Drawing.Point(1137, 13);
            this.iconMaximize.Name = "iconMaximize";
            this.iconMaximize.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.iconMaximize.Size = new System.Drawing.Size(29, 29);
            this.iconMaximize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.iconMaximize.TabIndex = 3;
            this.iconMaximize.TabStop = false;
            this.iconMaximize.Click += new System.EventHandler(this.iconMaximize_Click);
            // 
            // iconClose
            // 
            this.iconClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.animationMenuHide1.SetDecoration(this.iconClose, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.iconClose, BunifuAnimatorNS.DecorationType.None);
            this.iconClose.IconChar = FontAwesome.Sharp.IconChar.WindowClose;
            this.iconClose.IconColor = System.Drawing.Color.White;
            this.iconClose.IconSize = 29;
            this.iconClose.Location = new System.Drawing.Point(1172, 13);
            this.iconClose.Name = "iconClose";
            this.iconClose.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.iconClose.Size = new System.Drawing.Size(29, 29);
            this.iconClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.iconClose.TabIndex = 2;
            this.iconClose.TabStop = false;
            this.iconClose.Click += new System.EventHandler(this.iconClose_Click);
            // 
            // iconRestaure
            // 
            this.iconRestaure.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconRestaure.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.animationMenuHide1.SetDecoration(this.iconRestaure, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.iconRestaure, BunifuAnimatorNS.DecorationType.None);
            this.iconRestaure.IconChar = FontAwesome.Sharp.IconChar.WindowRestore;
            this.iconRestaure.IconColor = System.Drawing.Color.White;
            this.iconRestaure.IconSize = 29;
            this.iconRestaure.Location = new System.Drawing.Point(1137, 13);
            this.iconRestaure.Name = "iconRestaure";
            this.iconRestaure.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.iconRestaure.Size = new System.Drawing.Size(29, 29);
            this.iconRestaure.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.iconRestaure.TabIndex = 5;
            this.iconRestaure.TabStop = false;
            this.iconRestaure.Click += new System.EventHandler(this.iconRestaure_Click);
            // 
            // moverForm
            // 
            this.moverForm.Fixed = true;
            this.moverForm.Horizontal = true;
            this.moverForm.TargetControl = this.panelTopStandBy;
            this.moverForm.Vertical = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.panel1.Controls.Add(this.iconMenu);
            this.panel1.Controls.Add(this.panelMenu);
            this.animationMenuHide1.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(74, 720);
            this.panel1.TabIndex = 1;
            // 
            // iconMenu
            // 
            this.iconMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.animationMenuHide1.SetDecoration(this.iconMenu, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.iconMenu, BunifuAnimatorNS.DecorationType.None);
            this.iconMenu.IconChar = FontAwesome.Sharp.IconChar.Bars;
            this.iconMenu.IconColor = System.Drawing.Color.White;
            this.iconMenu.IconSize = 45;
            this.iconMenu.Location = new System.Drawing.Point(0, 10);
            this.iconMenu.Name = "iconMenu";
            this.iconMenu.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.iconMenu.Size = new System.Drawing.Size(59, 45);
            this.iconMenu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.iconMenu.TabIndex = 0;
            this.iconMenu.TabStop = false;
            this.iconMenu.Click += new System.EventHandler(this.iconMenu_Click);
            // 
            // panelMenu
            // 
            this.panelMenu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panelMenu.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelMenu.BackgroundImage")));
            this.panelMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelMenu.Controls.Add(this.btnLucros);
            this.panelMenu.Controls.Add(this.btnOrcamentos);
            this.panelMenu.Controls.Add(this.btnConcluidos);
            this.panelMenu.Controls.Add(this.btnClientes);
            this.panelMenu.Controls.Add(this.lblSS);
            this.panelMenu.Controls.Add(this.btnServicos);
            this.panelMenu.Controls.Add(this.bunifuSeparator1);
            this.panelMenu.Controls.Add(this.lblBemVindo);
            this.animationMenuHide1.SetDecoration(this.panelMenu, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.panelMenu, BunifuAnimatorNS.DecorationType.None);
            this.panelMenu.GradientBottomLeft = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(103)))));
            this.panelMenu.GradientBottomRight = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(103)))));
            this.panelMenu.GradientTopLeft = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(103)))));
            this.panelMenu.GradientTopRight = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.panelMenu.Location = new System.Drawing.Point(-2, 63);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Quality = 10;
            this.panelMenu.Size = new System.Drawing.Size(62, 645);
            this.panelMenu.TabIndex = 2;
            // 
            // btnLucros
            // 
            this.btnLucros.Active = false;
            this.btnLucros.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnLucros.BackColor = System.Drawing.Color.Transparent;
            this.btnLucros.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLucros.BorderRadius = 0;
            this.btnLucros.ButtonText = "LUCROS";
            this.btnLucros.Cursor = System.Windows.Forms.Cursors.Hand;
            this.animationMenuShow1.SetDecoration(this.btnLucros, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuHide1.SetDecoration(this.btnLucros, BunifuAnimatorNS.DecorationType.None);
            this.btnLucros.DisabledColor = System.Drawing.Color.Gray;
            this.btnLucros.Iconcolor = System.Drawing.Color.Transparent;
            this.btnLucros.Iconimage = global::PFC___StandBy_CSharp.Properties.Resources.icons8_money_bag_480px;
            this.btnLucros.Iconimage_right = null;
            this.btnLucros.Iconimage_right_Selected = null;
            this.btnLucros.Iconimage_Selected = null;
            this.btnLucros.IconMarginLeft = 0;
            this.btnLucros.IconMarginRight = 0;
            this.btnLucros.IconRightVisible = true;
            this.btnLucros.IconRightZoom = 0D;
            this.btnLucros.IconVisible = true;
            this.btnLucros.IconZoom = 70D;
            this.btnLucros.IsTab = false;
            this.btnLucros.Location = new System.Drawing.Point(0, 374);
            this.btnLucros.Name = "btnLucros";
            this.btnLucros.Normalcolor = System.Drawing.Color.Transparent;
            this.btnLucros.OnHovercolor = System.Drawing.Color.Black;
            this.btnLucros.OnHoverTextColor = System.Drawing.Color.White;
            this.btnLucros.selected = false;
            this.btnLucros.Size = new System.Drawing.Size(187, 58);
            this.btnLucros.TabIndex = 13;
            this.btnLucros.Text = "LUCROS";
            this.btnLucros.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnLucros.Textcolor = System.Drawing.Color.White;
            this.btnLucros.TextFont = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // btnOrcamentos
            // 
            this.btnOrcamentos.Active = false;
            this.btnOrcamentos.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnOrcamentos.BackColor = System.Drawing.Color.Transparent;
            this.btnOrcamentos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnOrcamentos.BorderRadius = 0;
            this.btnOrcamentos.ButtonText = "ORÇAMENTOS";
            this.btnOrcamentos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.animationMenuShow1.SetDecoration(this.btnOrcamentos, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuHide1.SetDecoration(this.btnOrcamentos, BunifuAnimatorNS.DecorationType.None);
            this.btnOrcamentos.DisabledColor = System.Drawing.Color.Gray;
            this.btnOrcamentos.Iconcolor = System.Drawing.Color.Transparent;
            this.btnOrcamentos.Iconimage = global::PFC___StandBy_CSharp.Properties.Resources.icons8_estimate_96px;
            this.btnOrcamentos.Iconimage_right = null;
            this.btnOrcamentos.Iconimage_right_Selected = null;
            this.btnOrcamentos.Iconimage_Selected = null;
            this.btnOrcamentos.IconMarginLeft = 0;
            this.btnOrcamentos.IconMarginRight = 0;
            this.btnOrcamentos.IconRightVisible = true;
            this.btnOrcamentos.IconRightZoom = 0D;
            this.btnOrcamentos.IconVisible = true;
            this.btnOrcamentos.IconZoom = 70D;
            this.btnOrcamentos.IsTab = false;
            this.btnOrcamentos.Location = new System.Drawing.Point(0, 294);
            this.btnOrcamentos.Name = "btnOrcamentos";
            this.btnOrcamentos.Normalcolor = System.Drawing.Color.Transparent;
            this.btnOrcamentos.OnHovercolor = System.Drawing.Color.Black;
            this.btnOrcamentos.OnHoverTextColor = System.Drawing.Color.White;
            this.btnOrcamentos.selected = false;
            this.btnOrcamentos.Size = new System.Drawing.Size(187, 58);
            this.btnOrcamentos.TabIndex = 12;
            this.btnOrcamentos.Text = "ORÇAMENTOS";
            this.btnOrcamentos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnOrcamentos.Textcolor = System.Drawing.Color.White;
            this.btnOrcamentos.TextFont = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // btnConcluidos
            // 
            this.btnConcluidos.Active = false;
            this.btnConcluidos.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnConcluidos.BackColor = System.Drawing.Color.Transparent;
            this.btnConcluidos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnConcluidos.BorderRadius = 0;
            this.btnConcluidos.ButtonText = "CONCLUIDOS";
            this.btnConcluidos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.animationMenuShow1.SetDecoration(this.btnConcluidos, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuHide1.SetDecoration(this.btnConcluidos, BunifuAnimatorNS.DecorationType.None);
            this.btnConcluidos.DisabledColor = System.Drawing.Color.Gray;
            this.btnConcluidos.Iconcolor = System.Drawing.Color.Transparent;
            this.btnConcluidos.Iconimage = global::PFC___StandBy_CSharp.Properties.Resources.icons8_checkmark_yes_512px;
            this.btnConcluidos.Iconimage_right = null;
            this.btnConcluidos.Iconimage_right_Selected = null;
            this.btnConcluidos.Iconimage_Selected = null;
            this.btnConcluidos.IconMarginLeft = 0;
            this.btnConcluidos.IconMarginRight = 0;
            this.btnConcluidos.IconRightVisible = true;
            this.btnConcluidos.IconRightZoom = 0D;
            this.btnConcluidos.IconVisible = true;
            this.btnConcluidos.IconZoom = 70D;
            this.btnConcluidos.IsTab = false;
            this.btnConcluidos.Location = new System.Drawing.Point(0, 215);
            this.btnConcluidos.Name = "btnConcluidos";
            this.btnConcluidos.Normalcolor = System.Drawing.Color.Transparent;
            this.btnConcluidos.OnHovercolor = System.Drawing.Color.Black;
            this.btnConcluidos.OnHoverTextColor = System.Drawing.Color.White;
            this.btnConcluidos.selected = false;
            this.btnConcluidos.Size = new System.Drawing.Size(187, 58);
            this.btnConcluidos.TabIndex = 11;
            this.btnConcluidos.Text = "CONCLUIDOS";
            this.btnConcluidos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnConcluidos.Textcolor = System.Drawing.Color.White;
            this.btnConcluidos.TextFont = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // btnClientes
            // 
            this.btnClientes.Active = false;
            this.btnClientes.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnClientes.BackColor = System.Drawing.Color.Transparent;
            this.btnClientes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClientes.BorderRadius = 0;
            this.btnClientes.ButtonText = "CLIENTES";
            this.btnClientes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.animationMenuShow1.SetDecoration(this.btnClientes, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuHide1.SetDecoration(this.btnClientes, BunifuAnimatorNS.DecorationType.None);
            this.btnClientes.DisabledColor = System.Drawing.Color.Gray;
            this.btnClientes.Iconcolor = System.Drawing.Color.Transparent;
            this.btnClientes.Iconimage = global::PFC___StandBy_CSharp.Properties.Resources.icons8_user_group_480px;
            this.btnClientes.Iconimage_right = null;
            this.btnClientes.Iconimage_right_Selected = null;
            this.btnClientes.Iconimage_Selected = null;
            this.btnClientes.IconMarginLeft = 0;
            this.btnClientes.IconMarginRight = 0;
            this.btnClientes.IconRightVisible = true;
            this.btnClientes.IconRightZoom = 0D;
            this.btnClientes.IconVisible = true;
            this.btnClientes.IconZoom = 70D;
            this.btnClientes.IsTab = false;
            this.btnClientes.Location = new System.Drawing.Point(0, 135);
            this.btnClientes.Name = "btnClientes";
            this.btnClientes.Normalcolor = System.Drawing.Color.Transparent;
            this.btnClientes.OnHovercolor = System.Drawing.Color.Black;
            this.btnClientes.OnHoverTextColor = System.Drawing.Color.White;
            this.btnClientes.selected = false;
            this.btnClientes.Size = new System.Drawing.Size(187, 58);
            this.btnClientes.TabIndex = 10;
            this.btnClientes.Text = "CLIENTES";
            this.btnClientes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnClientes.Textcolor = System.Drawing.Color.White;
            this.btnClientes.TextFont = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClientes.Click += new System.EventHandler(this.btnClientes_Click);
            // 
            // lblSS
            // 
            this.lblSS.AutoSize = true;
            this.lblSS.BackColor = System.Drawing.Color.Transparent;
            this.animationMenuShow1.SetDecoration(this.lblSS, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuHide1.SetDecoration(this.lblSS, BunifuAnimatorNS.DecorationType.None);
            this.lblSS.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSS.ForeColor = System.Drawing.Color.White;
            this.lblSS.Location = new System.Drawing.Point(8, 10);
            this.lblSS.Name = "lblSS";
            this.lblSS.Size = new System.Drawing.Size(49, 25);
            this.lblSS.TabIndex = 9;
            this.lblSS.Text = "S.S";
            // 
            // btnServicos
            // 
            this.btnServicos.Active = false;
            this.btnServicos.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnServicos.BackColor = System.Drawing.Color.Transparent;
            this.btnServicos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnServicos.BorderRadius = 0;
            this.btnServicos.ButtonText = "SERVIÇOS";
            this.btnServicos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.animationMenuShow1.SetDecoration(this.btnServicos, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuHide1.SetDecoration(this.btnServicos, BunifuAnimatorNS.DecorationType.None);
            this.btnServicos.DisabledColor = System.Drawing.Color.Gray;
            this.btnServicos.Iconcolor = System.Drawing.Color.Transparent;
            this.btnServicos.Iconimage = global::PFC___StandBy_CSharp.Properties.Resources.icons8_bulleted_list_480px;
            this.btnServicos.Iconimage_right = null;
            this.btnServicos.Iconimage_right_Selected = null;
            this.btnServicos.Iconimage_Selected = null;
            this.btnServicos.IconMarginLeft = 0;
            this.btnServicos.IconMarginRight = 0;
            this.btnServicos.IconRightVisible = true;
            this.btnServicos.IconRightZoom = 0D;
            this.btnServicos.IconVisible = true;
            this.btnServicos.IconZoom = 70D;
            this.btnServicos.IsTab = false;
            this.btnServicos.Location = new System.Drawing.Point(0, 55);
            this.btnServicos.Name = "btnServicos";
            this.btnServicos.Normalcolor = System.Drawing.Color.Transparent;
            this.btnServicos.OnHovercolor = System.Drawing.Color.Black;
            this.btnServicos.OnHoverTextColor = System.Drawing.Color.White;
            this.btnServicos.selected = false;
            this.btnServicos.Size = new System.Drawing.Size(187, 58);
            this.btnServicos.TabIndex = 8;
            this.btnServicos.Text = "SERVIÇOS";
            this.btnServicos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnServicos.Textcolor = System.Drawing.Color.White;
            this.btnServicos.TextFont = new System.Drawing.Font("Bahnschrift SemiCondensed", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnServicos.Click += new System.EventHandler(this.btnServicos_Click);
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.animationMenuHide1.SetDecoration(this.bunifuSeparator1, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.bunifuSeparator1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(3, 34);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(56, 15);
            this.bunifuSeparator1.TabIndex = 7;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // lblBemVindo
            // 
            this.lblBemVindo.AutoSize = true;
            this.lblBemVindo.BackColor = System.Drawing.Color.Transparent;
            this.animationMenuShow1.SetDecoration(this.lblBemVindo, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuHide1.SetDecoration(this.lblBemVindo, BunifuAnimatorNS.DecorationType.None);
            this.lblBemVindo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBemVindo.ForeColor = System.Drawing.Color.White;
            this.lblBemVindo.Location = new System.Drawing.Point(43, 10);
            this.lblBemVindo.Name = "lblBemVindo";
            this.lblBemVindo.Size = new System.Drawing.Size(91, 20);
            this.lblBemVindo.TabIndex = 6;
            this.lblBemVindo.Text = "Bem Vindo!";
            this.lblBemVindo.Visible = false;
            // 
            // ArredondarForm
            // 
            this.ArredondarForm.ElipseRadius = 7;
            this.ArredondarForm.TargetControl = this;
            // 
            // ArredondarMenu
            // 
            this.ArredondarMenu.ElipseRadius = 7;
            this.ArredondarMenu.TargetControl = this.panelMenu;
            // 
            // bunifuCards2
            // 
            this.bunifuCards2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCards2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.bunifuCards2.BorderRadius = 5;
            this.bunifuCards2.BottomSahddow = true;
            this.bunifuCards2.color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(103)))));
            this.bunifuCards2.Controls.Add(this.bunifuDataViz2);
            this.bunifuCards2.Controls.Add(this.lblQntServicosMensais);
            this.bunifuCards2.Controls.Add(this.bunifuCustomLabel2);
            this.animationMenuHide1.SetDecoration(this.bunifuCards2, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.bunifuCards2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCards2.LeftSahddow = false;
            this.bunifuCards2.Location = new System.Drawing.Point(0, 332);
            this.bunifuCards2.Name = "bunifuCards2";
            this.bunifuCards2.RightSahddow = true;
            this.bunifuCards2.ShadowDepth = 20;
            this.bunifuCards2.Size = new System.Drawing.Size(1194, 321);
            this.bunifuCards2.TabIndex = 2;
            // 
            // bunifuDataViz2
            // 
            this.bunifuDataViz2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuDataViz2.animationEnabled = false;
            this.bunifuDataViz2.AutoSize = true;
            this.bunifuDataViz2.AxisLineColor = System.Drawing.Color.White;
            this.bunifuDataViz2.AxisXFontColor = System.Drawing.Color.LightGray;
            this.bunifuDataViz2.AxisXGridColor = System.Drawing.Color.Gray;
            this.bunifuDataViz2.AxisXGridThickness = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.bunifuDataViz2.AxisYFontColor = System.Drawing.Color.SeaShell;
            this.bunifuDataViz2.AxisYGridColor = System.Drawing.Color.Gray;
            this.bunifuDataViz2.AxisYGridThickness = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.bunifuDataViz2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.bunifuDataViz2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.animationMenuHide1.SetDecoration(this.bunifuDataViz2, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.bunifuDataViz2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuDataViz2.Location = new System.Drawing.Point(0, 50);
            this.bunifuDataViz2.Name = "bunifuDataViz2";
            this.bunifuDataViz2.Size = new System.Drawing.Size(1191, 268);
            this.bunifuDataViz2.TabIndex = 0;
            this.bunifuDataViz2.Theme = Bunifu.DataViz.WinForms.BunifuDataViz._theme.theme3;
            this.bunifuDataViz2.Title = "";
            // 
            // lblQntServicosMensais
            // 
            this.lblQntServicosMensais.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQntServicosMensais.AutoSize = true;
            this.lblQntServicosMensais.BackColor = System.Drawing.Color.Transparent;
            this.animationMenuShow1.SetDecoration(this.lblQntServicosMensais, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuHide1.SetDecoration(this.lblQntServicosMensais, BunifuAnimatorNS.DecorationType.None);
            this.lblQntServicosMensais.Font = new System.Drawing.Font("Segoe UI Semibold", 30.25F, System.Drawing.FontStyle.Bold);
            this.lblQntServicosMensais.ForeColor = System.Drawing.Color.White;
            this.lblQntServicosMensais.Location = new System.Drawing.Point(1084, 0);
            this.lblQntServicosMensais.Name = "lblQntServicosMensais";
            this.lblQntServicosMensais.Size = new System.Drawing.Size(87, 55);
            this.lblQntServicosMensais.TabIndex = 3;
            this.lblQntServicosMensais.Text = "100";
            this.lblQntServicosMensais.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.BackColor = System.Drawing.Color.Transparent;
            this.animationMenuShow1.SetDecoration(this.bunifuCustomLabel2, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuHide1.SetDecoration(this.bunifuCustomLabel2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Segoe UI Semibold", 12.25F, System.Drawing.FontStyle.Bold);
            this.bunifuCustomLabel2.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(473, 14);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(140, 23);
            this.bunifuCustomLabel2.TabIndex = 1;
            this.bunifuCustomLabel2.Text = "Serviços Mensais";
            // 
            // lblQntServicosSemanais
            // 
            this.lblQntServicosSemanais.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQntServicosSemanais.AutoSize = true;
            this.lblQntServicosSemanais.BackColor = System.Drawing.Color.Transparent;
            this.animationMenuShow1.SetDecoration(this.lblQntServicosSemanais, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuHide1.SetDecoration(this.lblQntServicosSemanais, BunifuAnimatorNS.DecorationType.None);
            this.lblQntServicosSemanais.Font = new System.Drawing.Font("Segoe UI Semibold", 30.25F, System.Drawing.FontStyle.Bold);
            this.lblQntServicosSemanais.ForeColor = System.Drawing.Color.White;
            this.lblQntServicosSemanais.Location = new System.Drawing.Point(561, 0);
            this.lblQntServicosSemanais.Name = "lblQntServicosSemanais";
            this.lblQntServicosSemanais.Size = new System.Drawing.Size(87, 55);
            this.lblQntServicosSemanais.TabIndex = 2;
            this.lblQntServicosSemanais.Text = "100";
            this.lblQntServicosSemanais.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.BackColor = System.Drawing.Color.Transparent;
            this.animationMenuShow1.SetDecoration(this.bunifuCustomLabel1, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuHide1.SetDecoration(this.bunifuCustomLabel1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Segoe UI Semibold", 12.25F, System.Drawing.FontStyle.Bold);
            this.bunifuCustomLabel1.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(193, 13);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(150, 23);
            this.bunifuCustomLabel1.TabIndex = 1;
            this.bunifuCustomLabel1.Text = "Serviços Semanais";
            // 
            // bunifuDataViz1
            // 
            this.bunifuDataViz1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuDataViz1.animationEnabled = false;
            this.bunifuDataViz1.AxisLineColor = System.Drawing.Color.White;
            this.bunifuDataViz1.AxisXFontColor = System.Drawing.Color.LightGray;
            this.bunifuDataViz1.AxisXGridColor = System.Drawing.Color.Gray;
            this.bunifuDataViz1.AxisXGridThickness = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.bunifuDataViz1.AxisYFontColor = System.Drawing.Color.SeaShell;
            this.bunifuDataViz1.AxisYGridColor = System.Drawing.Color.Gray;
            this.bunifuDataViz1.AxisYGridThickness = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.bunifuDataViz1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.bunifuDataViz1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.animationMenuHide1.SetDecoration(this.bunifuDataViz1, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.bunifuDataViz1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuDataViz1.Location = new System.Drawing.Point(3, 43);
            this.bunifuDataViz1.Name = "bunifuDataViz1";
            this.bunifuDataViz1.Size = new System.Drawing.Size(664, 272);
            this.bunifuDataViz1.TabIndex = 0;
            this.bunifuDataViz1.Theme = Bunifu.DataViz.WinForms.BunifuDataViz._theme.theme1;
            this.bunifuDataViz1.Title = "";
            // 
            // bunifuCards3
            // 
            this.bunifuCards3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCards3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.bunifuCards3.BorderRadius = 5;
            this.bunifuCards3.BottomSahddow = true;
            this.bunifuCards3.color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(103)))));
            this.bunifuCards3.Controls.Add(this.bunifuCards7);
            this.bunifuCards3.Controls.Add(this.bunifuCards6);
            this.bunifuCards3.Controls.Add(this.bunifuCards5);
            this.bunifuCards3.Controls.Add(this.bunifuCards4);
            this.animationMenuHide1.SetDecoration(this.bunifuCards3, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.bunifuCards3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCards3.LeftSahddow = false;
            this.bunifuCards3.Location = new System.Drawing.Point(676, 8);
            this.bunifuCards3.Name = "bunifuCards3";
            this.bunifuCards3.RightSahddow = true;
            this.bunifuCards3.ShadowDepth = 20;
            this.bunifuCards3.Size = new System.Drawing.Size(518, 113);
            this.bunifuCards3.TabIndex = 4;
            // 
            // bunifuCards7
            // 
            this.bunifuCards7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCards7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.bunifuCards7.BorderRadius = 5;
            this.bunifuCards7.BottomSahddow = true;
            this.bunifuCards7.color = System.Drawing.Color.Transparent;
            this.bunifuCards7.Controls.Add(this.btnDesligarPc);
            this.animationMenuHide1.SetDecoration(this.bunifuCards7, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.bunifuCards7, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCards7.LeftSahddow = false;
            this.bunifuCards7.Location = new System.Drawing.Point(403, 10);
            this.bunifuCards7.Name = "bunifuCards7";
            this.bunifuCards7.RightSahddow = true;
            this.bunifuCards7.ShadowDepth = 20;
            this.bunifuCards7.Size = new System.Drawing.Size(106, 95);
            this.bunifuCards7.TabIndex = 5;
            // 
            // btnDesligarPc
            // 
            this.btnDesligarPc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDesligarPc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.btnDesligarPc.color = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.btnDesligarPc.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(103)))));
            this.btnDesligarPc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.animationMenuHide1.SetDecoration(this.btnDesligarPc, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.btnDesligarPc, BunifuAnimatorNS.DecorationType.None);
            this.btnDesligarPc.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDesligarPc.ForeColor = System.Drawing.Color.White;
            this.btnDesligarPc.Image = global::PFC___StandBy_CSharp.Properties.Resources.icons8_shutdown_96px;
            this.btnDesligarPc.ImagePosition = 4;
            this.btnDesligarPc.ImageZoom = 50;
            this.btnDesligarPc.LabelPosition = 34;
            this.btnDesligarPc.LabelText = "Desligar";
            this.btnDesligarPc.Location = new System.Drawing.Point(-1, 2);
            this.btnDesligarPc.Margin = new System.Windows.Forms.Padding(6);
            this.btnDesligarPc.Name = "btnDesligarPc";
            this.btnDesligarPc.Size = new System.Drawing.Size(106, 95);
            this.btnDesligarPc.TabIndex = 1;
            this.btnDesligarPc.Click += new System.EventHandler(this.bunifuTileButton3_Click);
            // 
            // bunifuCards6
            // 
            this.bunifuCards6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCards6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.bunifuCards6.BorderRadius = 5;
            this.bunifuCards6.BottomSahddow = true;
            this.bunifuCards6.color = System.Drawing.Color.Transparent;
            this.bunifuCards6.Controls.Add(this.bunifuTileButton2);
            this.animationMenuHide1.SetDecoration(this.bunifuCards6, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.bunifuCards6, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCards6.LeftSahddow = false;
            this.bunifuCards6.Location = new System.Drawing.Point(274, 10);
            this.bunifuCards6.Name = "bunifuCards6";
            this.bunifuCards6.RightSahddow = true;
            this.bunifuCards6.ShadowDepth = 20;
            this.bunifuCards6.Size = new System.Drawing.Size(106, 95);
            this.bunifuCards6.TabIndex = 5;
            // 
            // bunifuTileButton2
            // 
            this.bunifuTileButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuTileButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.bunifuTileButton2.color = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.bunifuTileButton2.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(103)))));
            this.bunifuTileButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.animationMenuHide1.SetDecoration(this.bunifuTileButton2, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.bunifuTileButton2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTileButton2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuTileButton2.ForeColor = System.Drawing.Color.White;
            this.bunifuTileButton2.Image = null;
            this.bunifuTileButton2.ImagePosition = 4;
            this.bunifuTileButton2.ImageZoom = 50;
            this.bunifuTileButton2.LabelPosition = 34;
            this.bunifuTileButton2.LabelText = "---------";
            this.bunifuTileButton2.Location = new System.Drawing.Point(-1, 2);
            this.bunifuTileButton2.Margin = new System.Windows.Forms.Padding(6);
            this.bunifuTileButton2.Name = "bunifuTileButton2";
            this.bunifuTileButton2.Size = new System.Drawing.Size(106, 95);
            this.bunifuTileButton2.TabIndex = 1;
            // 
            // bunifuCards5
            // 
            this.bunifuCards5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCards5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.bunifuCards5.BorderRadius = 5;
            this.bunifuCards5.BottomSahddow = true;
            this.bunifuCards5.color = System.Drawing.Color.Transparent;
            this.bunifuCards5.Controls.Add(this.bunifuTileButton1);
            this.animationMenuHide1.SetDecoration(this.bunifuCards5, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.bunifuCards5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCards5.LeftSahddow = false;
            this.bunifuCards5.Location = new System.Drawing.Point(140, 10);
            this.bunifuCards5.Name = "bunifuCards5";
            this.bunifuCards5.RightSahddow = true;
            this.bunifuCards5.ShadowDepth = 20;
            this.bunifuCards5.Size = new System.Drawing.Size(106, 95);
            this.bunifuCards5.TabIndex = 5;
            // 
            // bunifuTileButton1
            // 
            this.bunifuTileButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuTileButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.bunifuTileButton1.color = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.bunifuTileButton1.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(103)))));
            this.bunifuTileButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.animationMenuHide1.SetDecoration(this.bunifuTileButton1, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.bunifuTileButton1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTileButton1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuTileButton1.ForeColor = System.Drawing.Color.White;
            this.bunifuTileButton1.Image = null;
            this.bunifuTileButton1.ImagePosition = 4;
            this.bunifuTileButton1.ImageZoom = 50;
            this.bunifuTileButton1.LabelPosition = 34;
            this.bunifuTileButton1.LabelText = "---------";
            this.bunifuTileButton1.Location = new System.Drawing.Point(-1, 2);
            this.bunifuTileButton1.Margin = new System.Windows.Forms.Padding(6);
            this.bunifuTileButton1.Name = "bunifuTileButton1";
            this.bunifuTileButton1.Size = new System.Drawing.Size(106, 95);
            this.bunifuTileButton1.TabIndex = 1;
            // 
            // bunifuCards4
            // 
            this.bunifuCards4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCards4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.bunifuCards4.BorderRadius = 5;
            this.bunifuCards4.BottomSahddow = true;
            this.bunifuCards4.color = System.Drawing.Color.Transparent;
            this.bunifuCards4.Controls.Add(this.btnServicosPorMes);
            this.animationMenuHide1.SetDecoration(this.bunifuCards4, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.bunifuCards4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCards4.LeftSahddow = false;
            this.bunifuCards4.Location = new System.Drawing.Point(9, 10);
            this.bunifuCards4.Name = "bunifuCards4";
            this.bunifuCards4.RightSahddow = true;
            this.bunifuCards4.ShadowDepth = 20;
            this.bunifuCards4.Size = new System.Drawing.Size(106, 95);
            this.bunifuCards4.TabIndex = 4;
            // 
            // btnServicosPorMes
            // 
            this.btnServicosPorMes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnServicosPorMes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.btnServicosPorMes.color = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.btnServicosPorMes.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(103)))));
            this.btnServicosPorMes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.animationMenuHide1.SetDecoration(this.btnServicosPorMes, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.btnServicosPorMes, BunifuAnimatorNS.DecorationType.None);
            this.btnServicosPorMes.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnServicosPorMes.ForeColor = System.Drawing.Color.White;
            this.btnServicosPorMes.Image = global::PFC___StandBy_CSharp.Properties.Resources.icons8_list_512px;
            this.btnServicosPorMes.ImagePosition = 4;
            this.btnServicosPorMes.ImageZoom = 50;
            this.btnServicosPorMes.LabelPosition = 34;
            this.btnServicosPorMes.LabelText = "Serv/Mes";
            this.btnServicosPorMes.Location = new System.Drawing.Point(-1, 2);
            this.btnServicosPorMes.Margin = new System.Windows.Forms.Padding(6);
            this.btnServicosPorMes.Name = "btnServicosPorMes";
            this.btnServicosPorMes.Size = new System.Drawing.Size(106, 95);
            this.btnServicosPorMes.TabIndex = 1;
            this.btnServicosPorMes.Click += new System.EventHandler(this.btnServicosPorMes_Click);
            // 
            // bunifuCards8
            // 
            this.bunifuCards8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCards8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.bunifuCards8.BorderRadius = 5;
            this.bunifuCards8.BottomSahddow = true;
            this.bunifuCards8.color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(103)))));
            this.bunifuCards8.Controls.Add(this.lblBLUE);
            this.bunifuCards8.Controls.Add(this.lblGREEN);
            this.bunifuCards8.Controls.Add(this.lblRED);
            this.bunifuCards8.Controls.Add(this.btnResetarCor);
            this.bunifuCards8.Controls.Add(this.btnMudarCor);
            this.bunifuCards8.Controls.Add(this.track_BLUE);
            this.bunifuCards8.Controls.Add(this.track_GREEN);
            this.bunifuCards8.Controls.Add(this.track_RED);
            this.bunifuCards8.Controls.Add(this.panel_CorGeral);
            this.animationMenuHide1.SetDecoration(this.bunifuCards8, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.bunifuCards8, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCards8.LeftSahddow = false;
            this.bunifuCards8.Location = new System.Drawing.Point(676, 127);
            this.bunifuCards8.Name = "bunifuCards8";
            this.bunifuCards8.RightSahddow = true;
            this.bunifuCards8.ShadowDepth = 20;
            this.bunifuCards8.Size = new System.Drawing.Size(518, 199);
            this.bunifuCards8.TabIndex = 5;
            // 
            // lblBLUE
            // 
            this.lblBLUE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBLUE.AutoSize = true;
            this.animationMenuShow1.SetDecoration(this.lblBLUE, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuHide1.SetDecoration(this.lblBLUE, BunifuAnimatorNS.DecorationType.None);
            this.lblBLUE.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBLUE.ForeColor = System.Drawing.Color.Gainsboro;
            this.lblBLUE.Location = new System.Drawing.Point(457, 53);
            this.lblBLUE.Name = "lblBLUE";
            this.lblBLUE.Size = new System.Drawing.Size(37, 21);
            this.lblBLUE.TabIndex = 11;
            this.lblBLUE.Text = "255";
            // 
            // lblGREEN
            // 
            this.lblGREEN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGREEN.AutoSize = true;
            this.animationMenuShow1.SetDecoration(this.lblGREEN, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuHide1.SetDecoration(this.lblGREEN, BunifuAnimatorNS.DecorationType.None);
            this.lblGREEN.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGREEN.ForeColor = System.Drawing.Color.Gainsboro;
            this.lblGREEN.Location = new System.Drawing.Point(457, 33);
            this.lblGREEN.Name = "lblGREEN";
            this.lblGREEN.Size = new System.Drawing.Size(37, 21);
            this.lblGREEN.TabIndex = 10;
            this.lblGREEN.Text = "255";
            // 
            // lblRED
            // 
            this.lblRED.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRED.AutoSize = true;
            this.animationMenuShow1.SetDecoration(this.lblRED, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuHide1.SetDecoration(this.lblRED, BunifuAnimatorNS.DecorationType.None);
            this.lblRED.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRED.ForeColor = System.Drawing.Color.Gainsboro;
            this.lblRED.Location = new System.Drawing.Point(457, 12);
            this.lblRED.Name = "lblRED";
            this.lblRED.Size = new System.Drawing.Size(37, 21);
            this.lblRED.TabIndex = 9;
            this.lblRED.Text = "255";
            // 
            // btnResetarCor
            // 
            this.btnResetarCor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.animationMenuHide1.SetDecoration(this.btnResetarCor, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.btnResetarCor, BunifuAnimatorNS.DecorationType.None);
            this.btnResetarCor.Image = global::PFC___StandBy_CSharp.Properties.Resources.icons8_reset_480px;
            this.btnResetarCor.ImageActive = null;
            this.btnResetarCor.Location = new System.Drawing.Point(445, 136);
            this.btnResetarCor.Name = "btnResetarCor";
            this.btnResetarCor.Size = new System.Drawing.Size(64, 47);
            this.btnResetarCor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnResetarCor.TabIndex = 8;
            this.btnResetarCor.TabStop = false;
            this.btnResetarCor.Zoom = 10;
            this.btnResetarCor.Click += new System.EventHandler(this.btnResetarCor_Click);
            // 
            // btnMudarCor
            // 
            this.btnMudarCor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.animationMenuHide1.SetDecoration(this.btnMudarCor, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.btnMudarCor, BunifuAnimatorNS.DecorationType.None);
            this.btnMudarCor.Image = global::PFC___StandBy_CSharp.Properties.Resources.icons8_paint_roller_512px;
            this.btnMudarCor.ImageActive = null;
            this.btnMudarCor.Location = new System.Drawing.Point(445, 81);
            this.btnMudarCor.Name = "btnMudarCor";
            this.btnMudarCor.Size = new System.Drawing.Size(64, 47);
            this.btnMudarCor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMudarCor.TabIndex = 3;
            this.btnMudarCor.TabStop = false;
            this.btnMudarCor.Zoom = 10;
            this.btnMudarCor.Click += new System.EventHandler(this.btnMudarCor_Click);
            // 
            // track_BLUE
            // 
            this.track_BLUE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.track_BLUE.BackColor = System.Drawing.Color.Transparent;
            this.track_BLUE.BackgroudColor = System.Drawing.Color.LightGray;
            this.track_BLUE.BorderRadius = 0;
            this.animationMenuHide1.SetDecoration(this.track_BLUE, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.track_BLUE, BunifuAnimatorNS.DecorationType.None);
            this.track_BLUE.IndicatorColor = System.Drawing.Color.Blue;
            this.track_BLUE.Location = new System.Drawing.Point(9, 153);
            this.track_BLUE.MaximumValue = 255;
            this.track_BLUE.Name = "track_BLUE";
            this.track_BLUE.Size = new System.Drawing.Size(410, 30);
            this.track_BLUE.TabIndex = 7;
            this.track_BLUE.Value = 103;
            this.track_BLUE.ValueChanged += new System.EventHandler(this.track_BLUE_ValueChanged);
            this.track_BLUE.ValueChangeComplete += new System.EventHandler(this.track_BLUE_ValueChangeComplete);
            // 
            // track_GREEN
            // 
            this.track_GREEN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.track_GREEN.BackColor = System.Drawing.Color.Transparent;
            this.track_GREEN.BackgroudColor = System.Drawing.Color.LightGray;
            this.track_GREEN.BorderRadius = 0;
            this.animationMenuHide1.SetDecoration(this.track_GREEN, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.track_GREEN, BunifuAnimatorNS.DecorationType.None);
            this.track_GREEN.IndicatorColor = System.Drawing.Color.LimeGreen;
            this.track_GREEN.Location = new System.Drawing.Point(9, 117);
            this.track_GREEN.MaximumValue = 255;
            this.track_GREEN.Name = "track_GREEN";
            this.track_GREEN.Size = new System.Drawing.Size(410, 30);
            this.track_GREEN.TabIndex = 6;
            this.track_GREEN.Value = 0;
            this.track_GREEN.ValueChanged += new System.EventHandler(this.track_GREEN_ValueChanged);
            this.track_GREEN.ValueChangeComplete += new System.EventHandler(this.track_GREEN_ValueChangeComplete);
            // 
            // track_RED
            // 
            this.track_RED.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.track_RED.BackColor = System.Drawing.Color.Transparent;
            this.track_RED.BackgroudColor = System.Drawing.Color.LightGray;
            this.track_RED.BorderRadius = 0;
            this.animationMenuHide1.SetDecoration(this.track_RED, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.track_RED, BunifuAnimatorNS.DecorationType.None);
            this.track_RED.IndicatorColor = System.Drawing.Color.Red;
            this.track_RED.Location = new System.Drawing.Point(9, 81);
            this.track_RED.MaximumValue = 255;
            this.track_RED.Name = "track_RED";
            this.track_RED.Size = new System.Drawing.Size(410, 30);
            this.track_RED.TabIndex = 5;
            this.track_RED.Value = 255;
            this.track_RED.ValueChanged += new System.EventHandler(this.track_RED_ValueChanged);
            this.track_RED.ValueChangeComplete += new System.EventHandler(this.track_RED_ValueChangeComplete);
            // 
            // panel_CorGeral
            // 
            this.panel_CorGeral.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_CorGeral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(59)))));
            this.animationMenuHide1.SetDecoration(this.panel_CorGeral, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.panel_CorGeral, BunifuAnimatorNS.DecorationType.None);
            this.panel_CorGeral.Location = new System.Drawing.Point(9, 12);
            this.panel_CorGeral.Name = "panel_CorGeral";
            this.panel_CorGeral.Size = new System.Drawing.Size(410, 62);
            this.panel_CorGeral.TabIndex = 4;
            // 
            // panelCentral
            // 
            this.panelCentral.AutoSize = true;
            this.panelCentral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.panelCentral.Controls.Add(this.bunifuCards8);
            this.panelCentral.Controls.Add(this.bunifuCards3);
            this.panelCentral.Controls.Add(this.bunifuCards1);
            this.panelCentral.Controls.Add(this.bunifuCards2);
            this.animationMenuHide1.SetDecoration(this.panelCentral, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.panelCentral, BunifuAnimatorNS.DecorationType.None);
            this.panelCentral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCentral.Location = new System.Drawing.Point(74, 55);
            this.panelCentral.Name = "panelCentral";
            this.panelCentral.Size = new System.Drawing.Size(1206, 665);
            this.panelCentral.TabIndex = 2;
            // 
            // bunifuCards1
            // 
            this.bunifuCards1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCards1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(38)))), ((int)(((byte)(59)))));
            this.bunifuCards1.BorderRadius = 5;
            this.bunifuCards1.BottomSahddow = true;
            this.bunifuCards1.color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(103)))));
            this.bunifuCards1.Controls.Add(this.lblQntServicosSemanais);
            this.bunifuCards1.Controls.Add(this.bunifuCustomLabel1);
            this.bunifuCards1.Controls.Add(this.bunifuDataViz1);
            this.animationMenuHide1.SetDecoration(this.bunifuCards1, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this.bunifuCards1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCards1.LeftSahddow = false;
            this.bunifuCards1.Location = new System.Drawing.Point(0, 8);
            this.bunifuCards1.Name = "bunifuCards1";
            this.bunifuCards1.RightSahddow = true;
            this.bunifuCards1.ShadowDepth = 20;
            this.bunifuCards1.Size = new System.Drawing.Size(670, 318);
            this.bunifuCards1.TabIndex = 1;
            // 
            // bunifuColorTransition1
            // 
            this.bunifuColorTransition1.Color1 = System.Drawing.Color.White;
            this.bunifuColorTransition1.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(0)))), ((int)(((byte)(102)))));
            this.bunifuColorTransition1.ProgessValue = 0;
            // 
            // mudarCorGeral
            // 
            this.mudarCorGeral.Interval = 10;
            this.mudarCorGeral.Tick += new System.EventHandler(this.mudarCorGeral_Tick);
            // 
            // iniciarFormTransicao
            // 
            this.iniciarFormTransicao.Delay = 1;
            // 
            // animationMenuShow1
            // 
            this.animationMenuShow1.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.animationMenuShow1.Cursor = null;
            animation2.AnimateOnlyDifferences = true;
            animation2.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.BlindCoeff")));
            animation2.LeafCoeff = 0F;
            animation2.MaxTime = 1F;
            animation2.MinTime = 0F;
            animation2.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicCoeff")));
            animation2.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicShift")));
            animation2.MosaicSize = 0;
            animation2.Padding = new System.Windows.Forms.Padding(0);
            animation2.RotateCoeff = 0F;
            animation2.RotateLimit = 0F;
            animation2.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.ScaleCoeff")));
            animation2.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.SlideCoeff")));
            animation2.TimeCoeff = 0F;
            animation2.TransparencyCoeff = 0F;
            this.animationMenuShow1.DefaultAnimation = animation2;
            this.animationMenuShow1.Interval = 1;
            this.animationMenuShow1.MaxAnimationTime = 3000;
            // 
            // animationMenuHide1
            // 
            this.animationMenuHide1.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.animationMenuHide1.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 0F;
            this.animationMenuHide1.DefaultAnimation = animation1;
            this.animationMenuHide1.Interval = 1;
            this.animationMenuHide1.MaxAnimationTime = 3000;
            // 
            // form_StandBy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(46)))));
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.panelCentral);
            this.Controls.Add(this.panelTopStandBy);
            this.Controls.Add(this.panel1);
            this.animationMenuHide1.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.animationMenuShow1.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "form_StandBy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StandBy System";
            this.panelTopStandBy.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgbuttonTitulo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconMinimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconMaximize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconRestaure)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iconMenu)).EndInit();
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            this.bunifuCards2.ResumeLayout(false);
            this.bunifuCards2.PerformLayout();
            this.bunifuCards3.ResumeLayout(false);
            this.bunifuCards7.ResumeLayout(false);
            this.bunifuCards6.ResumeLayout(false);
            this.bunifuCards5.ResumeLayout(false);
            this.bunifuCards4.ResumeLayout(false);
            this.bunifuCards8.ResumeLayout(false);
            this.bunifuCards8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnResetarCor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMudarCor)).EndInit();
            this.panelCentral.ResumeLayout(false);
            this.bunifuCards1.ResumeLayout(false);
            this.bunifuCards1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelTopStandBy;
        private FontAwesome.Sharp.IconPictureBox iconMenu;
        private Bunifu.Framework.UI.BunifuDragControl moverForm;
        private FontAwesome.Sharp.IconPictureBox iconClose;
        private FontAwesome.Sharp.IconPictureBox iconMinimize;
        private FontAwesome.Sharp.IconPictureBox iconMaximize;
        private FontAwesome.Sharp.IconPictureBox iconRestaure;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuGradientPanel panelMenu;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private Bunifu.Framework.UI.BunifuCustomLabel lblBemVindo;
        private Bunifu.Framework.UI.BunifuFlatButton btnServicos;
        private Bunifu.Framework.UI.BunifuElipse ArredondarForm;
        private Bunifu.Framework.UI.BunifuElipse ArredondarMenu;
        private Bunifu.Framework.UI.BunifuCustomLabel lblSS;
        private Bunifu.Framework.UI.BunifuFlatButton btnLucros;
        private Bunifu.Framework.UI.BunifuFlatButton btnOrcamentos;
        private Bunifu.Framework.UI.BunifuFlatButton btnConcluidos;
        private Bunifu.Framework.UI.BunifuFlatButton btnClientes;
        private Bunifu.Framework.UI.BunifuColorTransition bunifuColorTransition1;
        private System.Windows.Forms.Timer mudarCorGeral;
        private System.Windows.Forms.Panel panelCentral;
        private Bunifu.Framework.UI.BunifuCards bunifuCards8;
        private System.Windows.Forms.Label lblBLUE;
        private System.Windows.Forms.Label lblGREEN;
        private System.Windows.Forms.Label lblRED;
        private Bunifu.Framework.UI.BunifuImageButton btnResetarCor;
        private Bunifu.Framework.UI.BunifuImageButton btnMudarCor;
        private Bunifu.Framework.UI.BunifuSlider track_BLUE;
        private Bunifu.Framework.UI.BunifuSlider track_GREEN;
        private Bunifu.Framework.UI.BunifuSlider track_RED;
        private System.Windows.Forms.Panel panel_CorGeral;
        private Bunifu.Framework.UI.BunifuCards bunifuCards3;
        private Bunifu.Framework.UI.BunifuCards bunifuCards7;
        private Bunifu.Framework.UI.BunifuTileButton btnDesligarPc;
        private Bunifu.Framework.UI.BunifuCards bunifuCards6;
        private Bunifu.Framework.UI.BunifuTileButton bunifuTileButton2;
        private Bunifu.Framework.UI.BunifuCards bunifuCards5;
        private Bunifu.Framework.UI.BunifuTileButton bunifuTileButton1;
        private Bunifu.Framework.UI.BunifuCards bunifuCards4;
        private Bunifu.Framework.UI.BunifuTileButton btnServicosPorMes;
        private Bunifu.Framework.UI.BunifuCustomLabel lblQntServicosSemanais;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.DataViz.WinForms.BunifuDataViz bunifuDataViz1;
        private Bunifu.Framework.UI.BunifuCards bunifuCards2;
        private Bunifu.DataViz.WinForms.BunifuDataViz bunifuDataViz2;
        private Bunifu.Framework.UI.BunifuCustomLabel lblQntServicosMensais;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private Bunifu.Framework.UI.BunifuFormFadeTransition iniciarFormTransicao;
        private BunifuAnimatorNS.BunifuTransition animationMenuShow1;
        private BunifuAnimatorNS.BunifuTransition animationMenuHide1;
        private Bunifu.Framework.UI.BunifuImageButton imgbuttonTitulo;
        private Bunifu.Framework.UI.BunifuCards bunifuCards1;
    }
}
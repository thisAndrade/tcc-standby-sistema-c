﻿namespace PFC___StandBy_CSharp.Forms
{
    partial class form_OrdensServico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.txtDefeitoOrdens = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSenhaOrdens = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAparelhoOrdens = new System.Windows.Forms.TextBox();
            this.txtSituacaoOrdens = new System.Windows.Forms.TextBox();
            this.grid_OrdensServicos = new System.Windows.Forms.DataGridView();
            this.btnCadastrarOrdem = new System.Windows.Forms.Button();
            this.cmbClientes = new System.Windows.Forms.ComboBox();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Aparelho = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Defeito = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Senha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Situacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grid_OrdensServicos)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(565, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 21);
            this.label3.TabIndex = 4;
            this.label3.Text = "DEFEITO";
            // 
            // txtDefeitoOrdens
            // 
            this.txtDefeitoOrdens.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDefeitoOrdens.Location = new System.Drawing.Point(569, 45);
            this.txtDefeitoOrdens.Name = "txtDefeitoOrdens";
            this.txtDefeitoOrdens.Size = new System.Drawing.Size(246, 26);
            this.txtDefeitoOrdens.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(329, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "APARELHO";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(832, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "SENHA";
            // 
            // txtSenhaOrdens
            // 
            this.txtSenhaOrdens.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSenhaOrdens.Location = new System.Drawing.Point(836, 45);
            this.txtSenhaOrdens.Name = "txtSenhaOrdens";
            this.txtSenhaOrdens.Size = new System.Drawing.Size(175, 26);
            this.txtSenhaOrdens.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "CLIENTE";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(487, 103);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 21);
            this.label5.TabIndex = 8;
            this.label5.Text = "SITUAÇÃO";
            // 
            // txtAparelhoOrdens
            // 
            this.txtAparelhoOrdens.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAparelhoOrdens.Location = new System.Drawing.Point(333, 45);
            this.txtAparelhoOrdens.Name = "txtAparelhoOrdens";
            this.txtAparelhoOrdens.Size = new System.Drawing.Size(210, 26);
            this.txtAparelhoOrdens.TabIndex = 3;
            // 
            // txtSituacaoOrdens
            // 
            this.txtSituacaoOrdens.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSituacaoOrdens.Location = new System.Drawing.Point(12, 127);
            this.txtSituacaoOrdens.Name = "txtSituacaoOrdens";
            this.txtSituacaoOrdens.Size = new System.Drawing.Size(999, 26);
            this.txtSituacaoOrdens.TabIndex = 9;
            // 
            // grid_OrdensServicos
            // 
            this.grid_OrdensServicos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid_OrdensServicos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Data,
            this.Nome,
            this.Aparelho,
            this.Defeito,
            this.Senha,
            this.Situacao});
            this.grid_OrdensServicos.Location = new System.Drawing.Point(12, 201);
            this.grid_OrdensServicos.Name = "grid_OrdensServicos";
            this.grid_OrdensServicos.ReadOnly = true;
            this.grid_OrdensServicos.Size = new System.Drawing.Size(1070, 466);
            this.grid_OrdensServicos.TabIndex = 10;
            // 
            // btnCadastrarOrdem
            // 
            this.btnCadastrarOrdem.Location = new System.Drawing.Point(1018, 37);
            this.btnCadastrarOrdem.Name = "btnCadastrarOrdem";
            this.btnCadastrarOrdem.Size = new System.Drawing.Size(64, 44);
            this.btnCadastrarOrdem.TabIndex = 11;
            this.btnCadastrarOrdem.Text = "Inserir";
            this.btnCadastrarOrdem.UseVisualStyleBackColor = true;
            this.btnCadastrarOrdem.Click += new System.EventHandler(this.btnCadastrarOrdem_Click);
            // 
            // cmbClientes
            // 
            this.cmbClientes.Font = new System.Drawing.Font("Century Gothic", 11.25F);
            this.cmbClientes.FormattingEnabled = true;
            this.cmbClientes.Location = new System.Drawing.Point(12, 45);
            this.cmbClientes.Name = "cmbClientes";
            this.cmbClientes.Size = new System.Drawing.Size(296, 28);
            this.cmbClientes.TabIndex = 12;
            // 
            // Data
            // 
            this.Data.DataPropertyName = "sv_data";
            this.Data.HeaderText = "Data";
            this.Data.Name = "Data";
            // 
            // Nome
            // 
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            // 
            // Aparelho
            // 
            this.Aparelho.DataPropertyName = "sv_aparelho";
            this.Aparelho.HeaderText = "Aparelho";
            this.Aparelho.Name = "Aparelho";
            // 
            // Defeito
            // 
            this.Defeito.DataPropertyName = "sv_defeito";
            this.Defeito.HeaderText = "Defeito";
            this.Defeito.Name = "Defeito";
            // 
            // Senha
            // 
            this.Senha.DataPropertyName = "sv_senha";
            this.Senha.HeaderText = "Senha";
            this.Senha.Name = "Senha";
            // 
            // Situacao
            // 
            this.Situacao.DataPropertyName = "sv_situacao";
            this.Situacao.HeaderText = "Situacao";
            this.Situacao.Name = "Situacao";
            // 
            // form_OrdensServico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.BackgroundImage = global::PFC___StandBy_CSharp.Properties.Resources.bg_ordensServicos;
            this.ClientSize = new System.Drawing.Size(1094, 679);
            this.Controls.Add(this.cmbClientes);
            this.Controls.Add(this.btnCadastrarOrdem);
            this.Controls.Add(this.grid_OrdensServicos);
            this.Controls.Add(this.txtSituacaoOrdens);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtSenhaOrdens);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDefeitoOrdens);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtAparelhoOrdens);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "form_OrdensServico";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ordens de Serviços";
            ((System.ComponentModel.ISupportInitialize)(this.grid_OrdensServicos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDefeitoOrdens;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSenhaOrdens;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAparelhoOrdens;
        private System.Windows.Forms.TextBox txtSituacaoOrdens;
        private System.Windows.Forms.DataGridView grid_OrdensServicos;
        private System.Windows.Forms.Button btnCadastrarOrdem;
        public System.Windows.Forms.ComboBox cmbClientes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Aparelho;
        private System.Windows.Forms.DataGridViewTextBoxColumn Defeito;
        private System.Windows.Forms.DataGridViewTextBoxColumn Senha;
        private System.Windows.Forms.DataGridViewTextBoxColumn Situacao;
    }
}
﻿using PFC___StandBy_CSharp.Dados;
using PFC___StandBy_CSharp.PreencherComponentes;
using PFC___StandBy_CSharp.SqlDbConnect;
using PFC___StandBy_CSharp.MsgBox;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PFC___StandBy_CSharp.Forms
{
    public partial class form_OrdensServico : Form
    {
        BuscarDados bd = new BuscarDados();
        InserirDados id = new InserirDados();
        PreencherComboBoxCliente pc = new PreencherComboBoxCliente();
        MensagensErro me = new MensagensErro();
        MensagensSucesso ms = new MensagensSucesso();
        PreencherTableOrdensServicos preencherTableServ = new PreencherTableOrdensServicos();

        public form_OrdensServico()
        {
            InitializeComponent();
            
            pc.Preencher(cmbClientes);
            preencherTableServ.Preencher(grid_OrdensServicos);
            //Preencher();
        }

        private void btnCadastrarOrdem_Click(object sender, EventArgs e)
        {
            //Pego a ID do cliente no banco de dados pelo nome dele na combobox.
            int _idCliente = bd.BuscarIdCliente(cmbClientes.SelectedItem.ToString());

            //Pego a data de hoje.
            DateTime data = DateTime.Now;

            try
            {
                //Insiro o servico com os dados.
                id.InserirServico(data, _idCliente, txtAparelhoOrdens.Text, txtDefeitoOrdens.Text, txtSenhaOrdens.Text, txtSituacaoOrdens.Text);

                //Reseto os campos.
                txtAparelhoOrdens.Text = "";
                txtDefeitoOrdens.Text = "";
                txtSenhaOrdens.Text = "";
                txtSituacaoOrdens.Text = "";

                //Mensagem de Conclusao
                ms.InserirServicoSucesso();
            }
            catch (Exception ex)
            {
                //Mensagem de Erro
                me.ErroInserirServico(ex);
            }            
        }
    }
}

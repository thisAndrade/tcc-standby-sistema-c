﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PFC___StandBy_CSharp.SqlDbConnect;
using PFC___StandBy_CSharp.MsgBox;
using System.Data;
using System.Windows.Forms;

namespace PFC___StandBy_CSharp.Dados
{
    class DeletarDados : conexao
    {
        MensagensErro mErro = new MensagensErro();
        MensagensSucesso mSucesso = new MensagensSucesso();
        public void DeletarServico(int _idServico)
        {
            try
            {
                using (SqlConnection con = OpenConnection())
                {
                    string query = "delete from tb_servicos where sv_id = @IdServico";

                    SqlCommand cmd = new SqlCommand(query, con);

                    cmd.Parameters.Add("@IdServico", SqlDbType.Int).Value = _idServico;

                    cmd.ExecuteNonQuery();
                    con.Close();
                    mSucesso.DeletarServicoSucesso();
                }
            }
            catch (Exception ex)
            {
                mErro.ErroDeletarServico(ex);
                //MessageBox.Show("ERRO: " + ex);
            }
            
        }
    }
}

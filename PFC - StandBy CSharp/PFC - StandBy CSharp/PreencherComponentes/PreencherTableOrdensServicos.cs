﻿using PFC___StandBy_CSharp.MsgBox;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PFC___StandBy_CSharp.PreencherComponentes
{
    class PreencherTableOrdensServicos : MensagensErro
    {
        public void Preencher(DataGridView tabelaServicos)
        {
            
            SqlConnection con = OpenConnection();
            SqlDataAdapter adapter = new SqlDataAdapter("Select * from tb_servicos", con);
            //SqlDataAdapter adapter = new SqlDataAdapter("Select sv_data, sv_aparelho, sv_defeito, sv_senha, sv_situacao from tb_servicos", con);
            DataTable datatable = new DataTable();
            adapter.Fill(datatable);

            tabelaServicos.AutoGenerateColumns = false;
            tabelaServicos.AllowUserToAddRows = false;
            tabelaServicos.AllowUserToResizeColumns = false;
            tabelaServicos.AllowUserToDeleteRows = false;
            tabelaServicos.DataSource = datatable;
            CloseConnection();
        }
    }
}

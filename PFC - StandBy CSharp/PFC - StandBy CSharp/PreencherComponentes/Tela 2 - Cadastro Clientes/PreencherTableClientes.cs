﻿using PFC___StandBy_CSharp.SqlDbConnect;
using PFC___StandBy_CSharp.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PFC___StandBy_CSharp.PreencherComponentes.Tela_2___Cadastro_Clientes
{
    class PreencherTableClientes : conexao
    {
        //form_CadastroClientes cadCliente = new form_CadastroClientes();
        public void Preencher(DataGridView tabelaClientes)
        {
            DataTable datatable = new DataTable();
            SqlConnection con = OpenConnection();
            string Query = "select cl_nome, cl_cpf, cl_telefone from tb_clientes";
            SqlDataAdapter adapter = new SqlDataAdapter(Query, con);

            adapter.SelectCommand.ExecuteNonQuery();
            adapter.Fill(datatable);

            tabelaClientes.DataSource = datatable;
            tabelaClientes.AutoGenerateColumns = false;
            tabelaClientes.AllowUserToAddRows = false;
            tabelaClientes.AllowUserToResizeColumns = true;
            tabelaClientes.AllowUserToDeleteRows = false;
            CloseConnection(); 
        }
    }
}

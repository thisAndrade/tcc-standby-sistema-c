﻿using PFC___StandBy_CSharp.SqlDbConnect;
using System.Data;
using System.Data.SqlClient;
using System;
using System.Globalization;
using Bunifu.DataViz.WinForms;
using DataPoint = Bunifu.DataViz.WinForms.DataPoint;
using System.Windows.Forms;
using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;
using System.Collections;

namespace PFC___StandBy_CSharp.Graficos
{

    class SemanaDoAno : conexao
    {
        int contadorDia = 0;
        int contadorServicos = 0;
        int qntServicos = 0;

        int contadorServicosMes = 0;
        public int BuscarSemanaAno(DateTime dataServico)
        {
            CultureInfo cultureInfo = new CultureInfo("en-US");
            Calendar meuCalendario = cultureInfo.Calendar;

            CalendarWeekRule RegraCalendarioSemanal = cultureInfo.DateTimeFormat.CalendarWeekRule;
            DayOfWeek PrimeiroDiaSemana = cultureInfo.DateTimeFormat.FirstDayOfWeek;

            string data = "2020-03-27";
            DateTime date = DateTime.Parse(data);

            int dia = meuCalendario.GetWeekOfYear(dataServico, RegraCalendarioSemanal, PrimeiroDiaSemana);

            return dia;
        }

        public int BuscarSemanaDatabase()
        {
            SqlConnection con = OpenConnection();
            SqlDataReader dr;
            string Query = "select * from tb_semanaAno";

            SqlCommand cmd = new SqlCommand(Query, con);
            dr = cmd.ExecuteReader();

            dr.Read();
            int dia = dr.GetInt32(0);

            return dia;
        }

        public void AtualizarSemanaAno(int _numeroSemana)
        {
            using (SqlConnection con = OpenConnection())
            {
                string Query = "update tb_semanaAno SET semanaDoAno = @NumeroSemana";

                SqlCommand cmd = new SqlCommand(Query, con);
                cmd.Parameters.AddWithValue("@NumeroSemana", _numeroSemana);
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void VerificarServicosSemanais(BunifuDataViz bunifuDataViz1, int _year, Label lblQntServicosSemanais)
        {
            try
            {
                DataPoint graficoArea, graficoLinha;
                graficoArea = new DataPoint(BunifuDataViz._type.Bunifu_area);
                graficoLinha = new DataPoint(BunifuDataViz._type.Bunifu_line);
                var canvas = new Canvas();
                int[] vetDiaSemana = new int[7] { 0, 0, 0, 0, 0, 0, 0 };
                string[] vetNomeDiaSemana = new string[14] { "n/a", "n/a", "n/a", "n/a", "n/a", "n/a", "n/a", "n/a", "n/a", "n/a", "n/a", "n/a", "n/a", "n/a" };

                
                using (SqlConnection con = OpenConnection())
                {
                    SqlDataReader dr;

                    //string Query = "select sv_id, sv_data from tb_servicos where year(sv_data) = @Ano order by sv_data";

                    SqlCommand cmd = new SqlCommand("SelecionarServicosAno", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Ano", SqlDbType.Int).Value = _year;
                    dr = cmd.ExecuteReader();

                    dr.Read();

                    int numSemanaAtual = BuscarSemanaAno(DateTime.Now);
                    //MessageBox.Show($"Estamos no dia {DateTime.Now} na semana {numSemanaAtual}");
                    AtualizarSemanaAno(numSemanaAtual);
                    int numSemanaDB = BuscarSemanaDatabase();

                    do
                    {
                        int numSemana = BuscarSemanaAno(dr.GetDateTime(1));
                        DateTime dateValue = dr.GetDateTime(1);
                        string dataAbreviada = dateValue.ToString("ddd", new CultureInfo("en-US"));

                        if (numSemana == numSemanaDB)
                        {
                            qntServicos++;
                            //DateTime dateValue = dr.GetDateTime(1);
                            //string dataAbreviada = dateValue.ToString("ddd", new CultureInfo("en-US"));
                            //MessageBox.Show($"Essa data | {dr.GetDateTime(1)} | e dessa semana: {numSemana} | dia: {dataAbreviada}");
                            //contador++;
                            //MessageBox.Show(contador.ToString());

                            if (vetNomeDiaSemana[contadorDia] != dataAbreviada && contadorDia < 1)
                            {
                                contadorServicos = 0;
                                //vetNomeDiaSemana[contadorDia] = dataAbreviada;
                            }
                            else if (vetNomeDiaSemana[contadorDia - 1] != dataAbreviada && contadorDia < 7)
                            {
                                contadorServicos++;
                                //vetNomeDiaSemana[contadorDia] = dataAbreviada;
                            }

                            switch (dataAbreviada)
                            {
                                case "Mon": //Segunda-Feira
                                    vetDiaSemana[contadorServicos] += +1;
                                    break;

                                case "Tue": //Terca-Feira
                                    vetDiaSemana[contadorServicos] += +1;
                                    break;

                                case "Wed": //Quarta-Feira
                                    vetDiaSemana[contadorServicos] += +1;
                                    break;

                                case "Thu": //Quinta-Feira
                                    vetDiaSemana[contadorServicos] += +1;
                                    break;

                                case "Fri": //Sexta-Feira
                                    vetDiaSemana[contadorServicos] += +1;
                                    break;

                                case "Sat": //Sabado
                                    vetDiaSemana[contadorServicos] += +1;
                                    break;

                                case "Sun": //Domingo
                                    vetDiaSemana[contadorServicos] += +1;
                                    break;
                            }

                            //Aqui eu atribuo os dias da semana onde existem dados
                            //e armazeno o nome do dia dentro do vetor, os dias que nao tem dados
                            //ficam como n/a no vetor.
                            if (vetNomeDiaSemana[contadorDia] != dataAbreviada && contadorDia < 1)
                            {
                                vetNomeDiaSemana[contadorDia] = dataAbreviada;
                                contadorDia++;
                            }
                            if (vetNomeDiaSemana[contadorDia - 1] != dataAbreviada && contadorDia < 7)
                            {

                                vetNomeDiaSemana[contadorDia] = dataAbreviada;
                                contadorDia++;
                            }
                        }
                    } while (dr.Read());
                    dr.Close();
                    //Verificar dias restantes da semana

                    //string nomeSemana = vetNomeDiaSemana[contadorDia - 1];
                    //switch (nomeSemana)
                    //{
                    //    case "Mon": //Segunda-Feira
                    //        vetNomeDiaSemana[contadorDia] = "Tue";
                    //        vetNomeDiaSemana[contadorDia + 1] = "Wed";
                    //        vetNomeDiaSemana[contadorDia + 2] = "Thu";
                    //        vetNomeDiaSemana[contadorDia + 3] = "Fri";
                    //        vetNomeDiaSemana[contadorDia + 4] = "Sat";
                    //        vetNomeDiaSemana[contadorDia + 5] = "Sun";
                    //        break;

                    //    case "Tue": //Terca-Feira
                    //        vetNomeDiaSemana[contadorDia] = "Wed";
                    //        vetNomeDiaSemana[contadorDia + 1] = "Thu";
                    //        vetNomeDiaSemana[contadorDia + 2] = "Fri";
                    //        vetNomeDiaSemana[contadorDia + 3] = "Sat";
                    //        vetNomeDiaSemana[contadorDia + 4] = "Sun";
                    //        vetNomeDiaSemana[contadorDia + 5] = "Mon";
                    //        break;

                    //    case "Wed": //Quarta-Feira
                    //        vetNomeDiaSemana[contadorDia] = "Thu";
                    //        vetNomeDiaSemana[contadorDia + 1] = "Fri";
                    //        vetNomeDiaSemana[contadorDia + 2] = "Sat";
                    //        vetNomeDiaSemana[contadorDia + 3] = "Sun";
                    //        vetNomeDiaSemana[contadorDia + 4] = "Mon";
                    //        vetNomeDiaSemana[contadorDia + 5] = "Tue";
                    //        break;

                    //    case "Thu": //Quinta-Feira
                    //        vetNomeDiaSemana[contadorDia] = "Fri";
                    //        vetNomeDiaSemana[contadorDia + 1] = "Sat";
                    //        vetNomeDiaSemana[contadorDia + 2] = "Sun";
                    //        vetNomeDiaSemana[contadorDia + 3] = "Mon";
                    //        vetNomeDiaSemana[contadorDia + 4] = "Tue";
                    //        vetNomeDiaSemana[contadorDia + 5] = "Wed";
                    //        break;

                    //    case "Fri": //Sexta-Feira
                    //        vetNomeDiaSemana[contadorDia] = "Sat";
                    //        vetNomeDiaSemana[contadorDia + 1] = "Sun";
                    //        vetNomeDiaSemana[contadorDia + 2] = "Mon";
                    //        vetNomeDiaSemana[contadorDia + 3] = "Tue";
                    //        vetNomeDiaSemana[contadorDia + 4] = "Wed";
                    //        vetNomeDiaSemana[contadorDia + 5] = "Thu";
                    //        break;

                    //    case "Sat": //Sabado
                    //        vetNomeDiaSemana[contadorDia] = "Sun";
                    //        vetNomeDiaSemana[contadorDia + 1] = "Mon";
                    //        vetNomeDiaSemana[contadorDia + 2] = "Tue";
                    //        vetNomeDiaSemana[contadorDia + 3] = "Wed";
                    //        vetNomeDiaSemana[contadorDia + 4] = "Thu";
                    //        vetNomeDiaSemana[contadorDia + 5] = "Fri";
                    //        break;

                    //    case "Sun": //Domingo
                    //        vetNomeDiaSemana[contadorDia] = "Mon";
                    //        vetNomeDiaSemana[contadorDia + 1] = "Tue";
                    //        vetNomeDiaSemana[contadorDia + 2] = "Wed";
                    //        vetNomeDiaSemana[contadorDia + 3] = "Thu";
                    //        vetNomeDiaSemana[contadorDia + 4] = "Fri";
                    //        vetNomeDiaSemana[contadorDia + 5] = "Sat";
                    //        break;
                    //}
                    for (int i = 0; i < 7; i++)
                    {
                        switch (vetNomeDiaSemana[i])
                        {
                            case "Mon":
                                vetNomeDiaSemana[i] = "SEGUNDA";
                                contadorDia++;
                                break;
                            case "Tue":
                                vetNomeDiaSemana[i] = "TERÇA";
                                contadorDia++;
                                break;
                            case "Wed":
                                vetNomeDiaSemana[i] = "QUARTA";
                                contadorDia++;
                                break;
                            case "Thu":
                                vetNomeDiaSemana[i] = "QUINTA";
                                contadorDia++;
                                break;
                            case "Fri":
                                vetNomeDiaSemana[i] = "SEXTA";
                                contadorDia++;
                                break;
                            case "Sat":
                                vetNomeDiaSemana[i] = "SABADO";
                                contadorDia++;
                                break;
                            case "Sun":
                                vetNomeDiaSemana[i] = "DOMINGO";
                                contadorDia++;
                                break;
                        }
                    }


                    graficoArea.addLabely(vetNomeDiaSemana[0].ToUpper(), vetDiaSemana[0]);
                    graficoArea.addLabely(vetNomeDiaSemana[1].ToUpper(), vetDiaSemana[1]);
                    graficoArea.addLabely(vetNomeDiaSemana[2].ToUpper(), vetDiaSemana[2]);
                    graficoArea.addLabely(vetNomeDiaSemana[3].ToUpper(), vetDiaSemana[3]);
                    graficoArea.addLabely(vetNomeDiaSemana[4].ToUpper(), vetDiaSemana[4]);
                    graficoArea.addLabely(vetNomeDiaSemana[5].ToUpper(), vetDiaSemana[5]);
                    graficoArea.addLabely(vetNomeDiaSemana[6].ToUpper(), vetDiaSemana[6]);

                    bunifuDataViz1.colorSet.Add(Color.FromArgb(0, 168, 235));
                    bunifuDataViz1.colorSet.Add(Color.White);

                    canvas.addData(graficoArea);

                    graficoLinha.addLabely(vetNomeDiaSemana[0].ToUpper(), vetDiaSemana[0]);
                    graficoLinha.addLabely(vetNomeDiaSemana[1].ToUpper(), vetDiaSemana[1]);
                    graficoLinha.addLabely(vetNomeDiaSemana[2].ToUpper(), vetDiaSemana[2]);
                    graficoLinha.addLabely(vetNomeDiaSemana[3].ToUpper(), vetDiaSemana[3]);
                    graficoLinha.addLabely(vetNomeDiaSemana[4].ToUpper(), vetDiaSemana[4]);
                    graficoLinha.addLabely(vetNomeDiaSemana[5].ToUpper(), vetDiaSemana[5]);
                    graficoLinha.addLabely(vetNomeDiaSemana[6].ToUpper(), vetDiaSemana[6]);
                    canvas.addData(graficoLinha);

                    bunifuDataViz1.Render(canvas);
                    lblQntServicosSemanais.Text = qntServicos.ToString();
                    //bunifuDataViz2.Render(canvas);
                    //MessageBox.Show(con.State.ToString());
                }
                //MessageBox.Show(con.State.ToString());
                //dr.Close();
                // con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERRO: " + ex);
            }
        }

        public void VerificarServicosMensais(BunifuDataViz bunifuDataViz2, int _mes, Label lblQntServicosMensais)
        {
            try
            {
                DataPoint graficoArea, graficoLinha;
                graficoArea = new DataPoint(BunifuDataViz._type.Bunifu_area);
                graficoLinha = new DataPoint(BunifuDataViz._type.Bunifu_line);
                var canvas = new Canvas();
                int[] vetServicosMes = new int[12] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                //string[] vetNomeMes = new string[12] { "JAN", "FEV", "MAR", "ABR", "MAI", "JUN", "JUL", "AGO", "SET", "OUT", "NOV", "DEZ" };
                string[] vetNomeMes = new string[12] { "JANEIRO", "FEVEREIRO", "MARÇO", "ABRIL", "MAIO", "JUNHO", "JULHO", "AGOSTO", "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO" };

                using (SqlConnection con = OpenConnection())
                {
                    SqlDataReader dr;

                    //string Query = "select sv_id, sv_data from tb_servicos where month(sv_data) = @Mes order by sv_data";
                    string Query = "select sv_id, sv_data from tb_servicos order by sv_data";
                    SqlCommand cmd = new SqlCommand(Query, con);
                    //cmd.Parameters.Add("@Mes", SqlDbType.Int).Value = _mes;
                    dr = cmd.ExecuteReader();

                    dr.Read();
                    do
                    {
                        contadorServicosMes++;
                        DateTime dateValue = dr.GetDateTime(1);
                        string mesNumero = dateValue.ToString("MM", new CultureInfo("en-US"));
                        //MessageBox.Show($"Estamos no Mes: {mesNumero}");

                        switch (mesNumero)
                        {
                            case "01": // Janeiro
                                vetServicosMes[0] += +1;
                                break;

                            case "02": // Fevereiro
                                vetServicosMes[1] += +1;
                                break;

                            case "03": // Marco
                                vetServicosMes[2] += +1;
                                break;

                            case "04": // Abril
                                vetServicosMes[3] += +1;
                                break;

                            case "05": // Maio
                                vetServicosMes[4] += +1;
                                break;

                            case "06": // Junho
                                vetServicosMes[5] += +1;
                                break;

                            case "07": // Julho
                                vetServicosMes[6] += +1;
                                break;

                            case "08": // Agosto
                                vetServicosMes[7] += +1;
                                break;

                            case "09": // Setembro
                                vetServicosMes[8] += +1;
                                break;

                            case "10": // Outubro
                                vetServicosMes[9] += +1;
                                break;

                            case "11": // Novembro
                                vetServicosMes[10] += +1;
                                break;

                            case "12": // Dezembro
                                vetServicosMes[11] += +1;
                                break;
                        }
                    } while (dr.Read());

                    for (int i = 0; i < 12; i++)
                    {
                        graficoArea.addLabely(vetNomeMes[i].ToUpper(), vetServicosMes[i]);
                    }


                    bunifuDataViz2.colorSet.Add(Color.FromArgb(0, 168, 235));
                    bunifuDataViz2.colorSet.Add(Color.White);

                    canvas.addData(graficoArea);

                    for (int i = 0; i < 12; i++)
                    {
                        graficoLinha.addLabely(vetNomeMes[i].ToUpper(), vetServicosMes[i]);
                    }

                    canvas.addData(graficoLinha);

                    lblQntServicosMensais.Text = contadorServicosMes.ToString();
                    bunifuDataViz2.Render(canvas);
                    //bunifuDataViz2.Render(canvas);
                    dr.Close();
                    //con.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERRO: " + ex);
            }
        }

        public void PreencherGraficoServicosPorMes(Chart grafico, int _year, Label lblAno)
        {
            //string[] MesesDoAno = new string[12] { "JAN", "FEV", "MAR", "ABR", "MAI", "JUN", "JUL", "AGO", "SET", "OUT", "NOV", "DEZ" };
            int[] qntPorMes = new int[12] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ArrayList servicosPorMes = new ArrayList();
            ArrayList mesesAno = new ArrayList
            {
                "JAN",
                "FEV",
                "MAR",
                "ABR",
                "MAI",
                "JUN",
                "JUL",
                "AGO",
                "SET",
                "OUT",
                "NOV",
                "DEZ"
            };

            using (SqlConnection con = OpenConnection())
            {
                SqlDataReader dr;
                string Query = "select sv_id, sv_data from tb_servicos where year(sv_data) = @Ano order by sv_data";
                SqlCommand cmd = new SqlCommand(Query, con);
                cmd.Parameters.Add("@Ano", SqlDbType.Int).Value = _year;

                dr = cmd.ExecuteReader();

                dr.Read();
                do
                {
                    if (dr.Read() == false)
                    {
                        break;
                    }
                    DateTime dateValue = dr.GetDateTime(1);
                    string mesNumero = dateValue.ToString("MM", new CultureInfo("en-US"));

                    switch (mesNumero)
                    {
                        case "01": // Janeiro
                            qntPorMes[0] += +1;
                            break;

                        case "02": // Fevereiro
                            qntPorMes[1] += +1;
                            break;

                        case "03": // Marco
                            qntPorMes[2] += +1;
                            break;

                        case "04": // Abril
                            qntPorMes[3] += +1;
                            break;

                        case "05": // Maio
                            qntPorMes[4] += +1;
                            break;

                        case "06": // Junho
                            qntPorMes[5] += +1;
                            break;

                        case "07": // Julho
                            qntPorMes[6] += +1;
                            break;

                        case "08": // Agosto
                            qntPorMes[7] += +1;
                            break;

                        case "09": // Setembro
                            qntPorMes[8] += +1;
                            break;

                        case "10": // Outubro
                            qntPorMes[9] += +1;
                            break;

                        case "11": // Novembro
                            qntPorMes[10] += +1;
                            break;

                        case "12": // Dezembro
                            qntPorMes[11] += +1;
                            break;
                    }

                } while (dr.Read());
                dr.Close();
                try
                {
                    for (int i = 0; i < 12; i++)
                    {
                        servicosPorMes.Add(qntPorMes[i]);
                    }
                    grafico.Series[0].Points.DataBindXY(mesesAno, servicosPorMes);
                    lblAno.Text = "ANO: " + _year.ToString();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.ToString());
                }
            }
            //dr.Close();
            //con.Close();
        }
    }
}
